part of template;

class ForgetPasswordLayout extends StatelessWidget {
  const ForgetPasswordLayout({Key key}) : super(key: key);

  static String routeName = "/forget_password";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pushNamed(context, LoginPage.routeName);
          },
          color: kPrimaryColor,
        ),
        centerTitle: true,
        title: Text(
          "Lupa Password",
          style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600),
        ),
      ),
      body: ForgetBody(),
    );
  }
}
