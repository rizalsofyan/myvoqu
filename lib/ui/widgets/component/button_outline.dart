part of template;

class OutlineButtonCustom extends StatelessWidget {
  const OutlineButtonCustom({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
        borderSide: BorderSide(width: 0.3),
        onPressed: () {},
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        child: Text(
          "Pesan",
        ));
  }
}
