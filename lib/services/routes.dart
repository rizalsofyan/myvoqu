import 'package:flutter/widgets.dart';
import 'package:quranku/data/dummy.dart';

import 'package:quranku/ui/template.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  LandingPage.routeName: (context) => LandingPage(),
  LoginPage.routeName: (context) => LoginPage(),
  ForgetPasswordLayout.routeName: (context) => ForgetPasswordLayout(),
  LoginSuccessLayout.routeName: (context) => LoginSuccessLayout(),
  RegisterPage.routeName: (context) => RegisterPage(),
  RegisterPageJenis.routeName: (context) => RegisterPageJenis(),
  RegisterPagePassword.routeName: (context) => RegisterPagePassword(),
  RegisterPagePhoto.routeName: (context) => RegisterPagePhoto(),
  RegisterSuccessLayout.routeName: (context) => RegisterSuccessLayout(),
  TermConditionPage.routeName: (context) => TermConditionPage(),
  HomePage.routeName: (context) => HomePage(),
  GroupPage.routeName: (context) => GroupPage(),
  SearchPage.routeName: (context) => SearchPage(),
  DetailUser.routeName: (context) => DetailUser(),
  DetailFollower.routeName: (context) => DetailFollower(),
  SideBarMenu.routeName: (context) => SideBarMenu(currentUser: currentUser),
  GroupInviteNotification.routeName: (context) => GroupInviteNotification(),
  GroupDetailLayout.routeName: (context) => GroupDetailLayout(),
  GroupInfoDetail.routeName: (context) => GroupInfoDetail(),
  GroupTaskLayout.routeName: (context) => GroupTaskLayout(),
  GroupMemberLayout.routeName: (context) => GroupMemberLayout(),
  GroupChattingLayout.routeName: (context) => GroupChattingLayout(),
};
