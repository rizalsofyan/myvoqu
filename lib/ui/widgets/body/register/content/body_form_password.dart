part of template;

class RegisterFormPassword extends StatefulWidget {
  const RegisterFormPassword({Key key}) : super(key: key);

  @override
  _RegisterFormPasswordState createState() => _RegisterFormPasswordState();
}

class _RegisterFormPasswordState extends State<RegisterFormPassword> {
  String password;
  String confirm;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          SizedBox(
            height: getProportionalScreenHeight(20),
          ),
          PasswordFormField(
            secureText: true,
            hint: "Buat Password",
            label: "Passwprd",
          ),
          SizedBox(
            height: getProportionalScreenHeight(20),
          ),
          PasswordFormField(
            secureText: true,
            hint: "Masukan Ulang Password",
            label: "Konfirmasi Password",
          ),
          SizedBox(
            height: getProportionalScreenHeight(20),
          ),
          SizedBox(
            height: getProportionalScreenHeight(20),
          ),
          DefaultButton(
            text: "Lanjut",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                Navigator.pushNamed(context, RegisterPagePhoto.routeName);
              }
            },
          ),
        ],
      ),
    );
  }
}
