part of template;

class SocialMediaCard extends StatelessWidget {
  const SocialMediaCard({
    Key key,
    this.icon,
    this.press,
  }) : super(key: key);
  final String icon;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(5)),
        padding: EdgeInsets.all(getProportioanalScreenWidth(2)),
        height: getProportionalScreenHeight(50),
        width: getProportioanalScreenWidth(50),
        decoration:
            BoxDecoration(color: Color(0xFFF5F6F9), shape: BoxShape.circle),
        child: SvgPicture.asset(icon),
      ),
    );
  }
}
