part of template;

class RegisterFormJenis extends StatefulWidget {
  const RegisterFormJenis({Key key}) : super(key: key);

  @override
  _RegisterFormJenisState createState() => _RegisterFormJenisState();
}

class _RegisterFormJenisState extends State<RegisterFormJenis> {
  String jPengguna;
  String jKelamin;
  List<String> _lPengguna = ["Premium", "Free"];
  List<String> _lKelamin = ["Laki-Laki", "Perempuan"];

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            buildDropdownButtonFormField(
                jPengguna,
                "Jenis Pengguna",
                "Jenis Pengguna",
                _lPengguna,
                (value) => setState(() {
                      jPengguna = value;
                    })),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            buildDropdownButtonFormField(
                jKelamin,
                "Jenis Kelamin",
                "Jenis Kelamin",
                _lKelamin,
                (value) => setState(() {
                      jKelamin = value;
                    })),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            DefaultButton(
              text: "Lanjut",
              press: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  Navigator.pushNamed(context, RegisterPagePassword.routeName);
                }
              },
            ),
          ],
        ));
  }
}
