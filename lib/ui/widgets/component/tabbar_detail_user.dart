part of template;

class TabbarDetailCustom extends StatelessWidget {
  const TabbarDetailCustom({
    Key key,
    @required this.press,
    this.color,
    this.widthSize,
    this.text,
  }) : super(key: key);

  final Function press;
  final Color color;
  final double widthSize;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: (SizeConfig.screenWidth * widthSize),
        child: IconButton(
          onPressed: press,
          splashColor: Colors.grey[50],
          icon: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: color,
            ),
          ),
        ));
  }
}
