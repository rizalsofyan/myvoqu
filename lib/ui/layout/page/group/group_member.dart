part of template;

class GroupMemberLayout extends StatefulWidget {
  const GroupMemberLayout({Key key}) : super(key: key);
  static String routeName = "/group_member";

  @override
  _GroupMemberLayoutState createState() => _GroupMemberLayoutState();
}

class _GroupMemberLayoutState extends State<GroupMemberLayout> {
  List<GroupMemberModel> groupMember = groupMembers;
  User user;
  String query = '';
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    groupMember = groupMembers;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          inputDecorationTheme: null,
        ),
        child: Scaffold(
            appBar: appBarGroup(context, "Anggota", false, null, null),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            floatingActionButton: InkWell(
              onTap: () {},
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    blurRadius: 4,
                    spreadRadius: 0.1,
                    color: Colors.grey[400].withOpacity(0.7),
                    offset: Offset(0, 4),
                  )
                ]),
                child:
                    Icon(MdiIcons.plusCircle, size: 40, color: kPrimaryColor),
              ),
            ),
            body: Container(
              child: Column(
                children: [
                  Container(
                      child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            TabbarDetailCustom(
                              press: () {
                                setState(() {
                                  selectedIndex = 0;
                                });
                              },
                              text: "Qori",
                              widthSize: 0.5,
                              color: selectedIndex == 0
                                  ? kPrimaryColor
                                  : kTextGrey,
                            ),
                            TabbarDetailCustom(
                              press: () {
                                setState(() {
                                  selectedIndex = 1;
                                });
                              },
                              text: "Penghafal",
                              widthSize: 0.5,
                              color: selectedIndex == 1
                                  ? kPrimaryColor
                                  : kTextGrey,
                            ),
                          ],
                        ),
                      ),
                      Container(
                          child: Row(
                        children: [
                          Container(
                            height: 2,
                            width: SizeConfig.screenWidth * 0.5,
                            decoration: BoxDecoration(
                              color: selectedIndex == 0
                                  ? kPrimaryColor
                                  : Colors.transparent,
                            ),
                          ),
                          Container(
                            height: 2,
                            width: SizeConfig.screenWidth * 0.5,
                            decoration: BoxDecoration(
                              color: selectedIndex == 1
                                  ? kPrimaryColor
                                  : Colors.transparent,
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  Expanded(
                      child: SingleChildScrollView(
                          child: Container(
                              child: ListView(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  children: [
                        IndexedStack(
                          index: selectedIndex,
                          children: [
                            bodyMemberList("qori"),
                            bodyMemberList("penghafal")
                          ],
                        ),
                      ])))),
                ],
              ),
            )));
  }

  Container bodyMemberList(String role) {
    return Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                  vertical: getProportioanalScreenWidth(20)),
              child: SearchWidget(
                hintText: "Cari",
                text: query,
                onChanged: searchMember,
              ),
            ),
            Container(
                child: Column(
              children: [
                ...List.generate(groupMember.length, (index) {
                  return groupMember[index].roleMember.toLowerCase() == role
                      ? InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, DetailFollower.routeName,
                              arguments: FollowerDetailArgument(
                                  listFriend: groupMember[index].user)),
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                vertical: getProportioanalScreenWidth(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    child: Row(children: [
                                  CircleAvatar(
                                    radius: 25,
                                    backgroundImage: CachedNetworkImageProvider(
                                        groupMember[index].user.fotoURl),
                                    backgroundColor: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: getProportioanalScreenWidth(10),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          "${groupMember[index].user.nickname}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold)),
                                      Text("${groupMember[index].user.name}",
                                          style: TextStyle(color: kTextGrey)),
                                    ],
                                  ),
                                ])),
                                groupMember[index].user.isFollow != null
                                    ? Container(
                                        child: FlatButtonCustom(
                                        press: () {
                                          setState(() {
                                            groupMember[index]
                                              ..user.isFollow =
                                                  !groupMember[index]
                                                      .user
                                                      .isFollow;
                                          });
                                        },
                                        color: groupMember[index].user.isFollow
                                            ? Colors.white
                                            : kPrimaryColor,
                                        shape: groupMember[index].user.isFollow
                                            ? RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                side: BorderSide(width: 0.3))
                                            : RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                        text: groupMember[index].user.isFollow
                                            ? "Hapus"
                                            : "Ikuti",
                                        colorText:
                                            groupMember[index].user.isFollow
                                                ? Colors.black
                                                : Colors.white,
                                      ))
                                    : SizedBox.shrink(),
                              ],
                            ),
                          ),
                        )
                      : SizedBox.shrink();
                })
              ],
            )),
          ],
        ));
  }

  void searchMember(String text) {
    final list = groupMembers
        .where((element) =>
            element.user.name.toLowerCase().contains(text.toLowerCase()))
        .toList();
    setState(() {
      this.query = text;
      this.groupMember = list;
    });
  }
}
