part of template;

class LoginPage extends StatelessWidget {
  // const LoginPage({ Key? key }) : super(key: key);
  static String routeName = "/login";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: buildAppBarLogin(context),
        body: LoginBodyHeader(
          content: LoginFormContent(),
        ));
  }
}
