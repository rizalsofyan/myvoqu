part of template;

class GroupPage extends StatefulWidget {
  const GroupPage({Key key}) : super(key: key);
  static String routeName = "/group";

  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  bool iconEmpty;
  int selectedIndex = 0;
  List<GroupListModel> grouplist = groupLists;
  String query = '';

  List<Widget> listTab = [
    Tab(
      child: Row(
        children: [
          Text("Grup Saya"),
          SizedBox(width: getProportioanalScreenWidth(2)),
          Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.blue[900]),
              child: Center(
                  child: Text((groupLists.length - 1).toString(),
                      style: TextStyle(color: Colors.white, fontSize: 10),
                      textAlign: TextAlign.center))),
        ],
      ),
    ),
    Tab(
      child: Row(
        children: [
          Text("Grup Populer"),
          SizedBox(width: getProportioanalScreenWidth(1)),
          Icon(MdiIcons.fire, size: 30, color: Colors.orange[400]),
        ],
      ),
    ),
    Tab(
      child: Text("Semua Grup"),
    ),
  ];
  @override
  void initState() {
    super.initState();
    grouplist = groupLists;
    tabController = TabController(length: listTab.length, vsync: this);
    this.tabController.addListener(() {
      setState(() {
        selectedIndex = tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        inputDecorationTheme: null,
      ),
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: GestureDetector(
          onTap: () =>
              Navigator.pushNamed(context, GroupInviteNotification.routeName),
          child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  blurRadius: 3,
                  offset: Offset(0, 0), // Shadow position
                ),
              ],
            ),
            child: Stack(alignment: Alignment.center, children: [
              Container(
                child: Icon(MdiIcons.emailOpenMultipleOutline,
                    color: Colors.black, size: getProportioanalScreenWidth(35)),
              ),
              Positioned(
                  bottom: 5,
                  right: 5,
                  child: Container(
                      width: 15.0,
                      height: 15.0,
                      decoration: BoxDecoration(
                        color: kPrimaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Text(
                          "1",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                      )))
            ]),
          ),
        ),
        appBar: AppBar(
          elevation: 0,
          toolbarHeight: getProportioanalScreenWidth(150),
          title: Text("Grup Hafalan"),
          leading: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: getProportioanalScreenWidth(5),
                  vertical: getProportioanalScreenWidth(20)),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(50))),
              child: IconButton(
                iconSize: 28.0,
                icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
                onPressed: () =>
                    Navigator.pushNamed(context, SideBarMenu.routeName),
              )),
          bottom: TabBar(
            controller: tabController,
            tabs: listTab,
            labelColor: Colors.blue[900],
            unselectedLabelColor: Colors.white,
            indicatorColor: Colors.blue[900],
            indicatorSize: TabBarIndicatorSize.tab,
            isScrollable: true,
            indicatorWeight: 4.0,
            onTap: (index) {},
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: TabBarView(controller: tabController, children: [
                  SingleChildScrollView(
                    child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: getProportioanalScreenWidth(10),
                            vertical: getProportioanalScreenWidth(10)),
                        child: Column(
                          children: [
                            SearchWidget(
                              hintText: "Cari Grup",
                              text: query,
                              onChanged: searchGroup,
                            ),
                            ...List.generate(grouplist.length, (index) {
                              return Container(
                                child: Column(
                                  children: [
                                    GroupCardList(
                                      titleName: grouplist[index].name,
                                      nameAuthor: grouplist[index].createdBy,
                                      genderType: grouplist[index].forGender,
                                      rateGroup: grouplist[index].rateGroup,
                                      memberCount:
                                          groupLists[index].numberMemberCount,
                                      imageGroup: groupLists[index].imgGroup,
                                      argument: GroupDetailArgumen(
                                          groupList: grouplist[index]),
                                    ),
                                    Divider(height: 5.0, color: Colors.grey),
                                  ],
                                ),
                              );
                            })
                          ],
                        )),
                  ),
                  SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: getProportioanalScreenWidth(10),
                          vertical: getProportioanalScreenWidth(10)),
                      child: Column(
                        children: [
                          SearchWidget(
                            hintText: "Cari Grup",
                            text: query,
                            onChanged: searchGroup,
                          ),
                          ...List.generate(grouplist.length, (index) {
                            return Container(
                              child: Column(
                                children: [
                                  GroupCardList(
                                    titleName: grouplist[index].name,
                                    nameAuthor: grouplist[index].createdBy,
                                    genderType: grouplist[index].forGender,
                                    rateGroup: grouplist[index].rateGroup,
                                    memberCount:
                                        grouplist[index].numberMemberCount,
                                    imageGroup: grouplist[index].imgGroup,
                                  ),
                                  Divider(height: 5.0, color: Colors.grey),
                                ],
                              ),
                            );
                          })
                        ],
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: getProportioanalScreenWidth(10),
                          vertical: getProportioanalScreenWidth(10)),
                      child: Column(
                        children: [
                          SearchWidget(
                            hintText: "Cari Grup",
                            text: query,
                            onChanged: searchGroup,
                          ),
                          ...List.generate(grouplist.length, (index) {
                            return Container(
                              child: Column(
                                children: [
                                  GroupCardList(
                                    titleName: grouplist[index].name,
                                    nameAuthor: grouplist[index].createdBy,
                                    genderType: grouplist[index].forGender,
                                    rateGroup: grouplist[index].rateGroup,
                                    memberCount:
                                        grouplist[index].numberMemberCount,
                                    imageGroup: grouplist[index].imgGroup,
                                    argument: GroupDetailArgumen(
                                        groupList: grouplist[index]),
                                  ),
                                  Divider(height: 5.0, color: Colors.grey),
                                ],
                              ),
                            );
                          })
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void searchGroup(String query) {
    final groups = groupLists.where((element) {
      return element.name.toLowerCase().contains(query.toLowerCase()) ||
          element.createdBy.toLowerCase().contains(query.toLowerCase());
    }).toList();
    setState(() {
      this.query = query;
      this.grouplist = groups;
    });
  }
}

class GroupCardList extends StatelessWidget {
  const GroupCardList({
    Key key,
    @required this.imageGroup,
    this.titleName,
    this.nameAuthor,
    this.genderType,
    this.rateGroup,
    this.memberCount,
    this.argument,
  }) : super(key: key);
  final String titleName;
  final String nameAuthor;
  final String genderType;
  final double rateGroup;
  final int memberCount;
  final String imageGroup;
  final Object argument;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, GroupDetailLayout.routeName,
          arguments: argument),
      child: Container(
        width: SizeConfig.screenWidth,
        margin: EdgeInsets.symmetric(vertical: getProportioanalScreenWidth(15)),
        // height: SizeConfig.screenWidth * 0.22,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                      width: SizeConfig.screenWidth * 0.2,
                      height: SizeConfig.screenWidth * 0.2,
                      decoration: BoxDecoration(
                        image: imageGroup.isEmpty
                            ? DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage(
                                    "assets/images/background_group_img.png"),
                              )
                            : DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                    "assets/images/background_dummy.png"),
                              ),
                      )),
                  SizedBox(
                    width: getProportioanalScreenWidth(5),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(titleName,
                          style: TextStyle(
                              fontFamily: "Balooda",
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                      Text(nameAuthor,
                          style: TextStyle(fontSize: 10, color: kTextGrey)),
                      SizedBox(
                        height: getProportioanalScreenWidth(10),
                      ),
                      Row(
                        children: [
                          Container(
                              child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                MdiIcons.genderMale,
                                size: 18,
                                color: Colors.blue[900],
                              ),
                              Text(genderType,
                                  style: TextStyle(
                                    fontSize: 10,
                                  )),
                            ],
                          )),
                          SizedBox(
                            width: getProportioanalScreenWidth(10),
                          ),
                          Container(
                              child: Row(
                            children: [
                              Icon(MdiIcons.star,
                                  size: 18, color: Colors.yellow[800]),
                              Text(rateGroup.toString(),
                                  style: TextStyle(
                                    fontSize: 10,
                                  )),
                            ],
                          )),
                          SizedBox(
                            width: getProportioanalScreenWidth(10),
                          ),
                          Container(
                              child: Row(
                            children: [
                              Icon(MdiIcons.account,
                                  size: 18, color: kPrimaryColor),
                              Text(memberCount.toString(),
                                  style: TextStyle(
                                    fontSize: 10,
                                  )),
                            ],
                          ))
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
            GestureDetector(
              child: Icon(Icons.more_vert, size: 30),
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    context: context,
                    builder: (context) {
                      return Stack(
                        children: [
                          Container(
                            height: SizeConfig.screenHeight * 0.4,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20)),
                              color: Colors.white,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          vertical:
                                              getProportioanalScreenWidth(40),
                                          horizontal:
                                              getProportioanalScreenWidth(10)),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              width:
                                                  SizeConfig.screenWidth * 0.2,
                                              height:
                                                  SizeConfig.screenWidth * 0.2,
                                              decoration: BoxDecoration(
                                                image: imageGroup.isEmpty
                                                    ? DecorationImage(
                                                        fit: BoxFit.contain,
                                                        image: AssetImage(
                                                            "assets/images/background_group_img.png"),
                                                      )
                                                    : DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: AssetImage(
                                                            imageGroup),
                                                      ),
                                              )),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(titleName,
                                                  style: TextStyle(
                                                      fontFamily: "Balooda",
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(nameAuthor,
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      color: kTextGrey)),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    Icon(MdiIcons.information,
                                        color: Colors.blue[200], size: 30),
                                    SizedBox(
                                      width: getProportioanalScreenWidth(5),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal:
                                          getProportioanalScreenWidth(10)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Material(
                                          color: Colors.transparent,
                                          child: Ink(
                                              width: SizeConfig.screenWidth,
                                              height:
                                                  getProportioanalScreenWidth(
                                                      40),
                                              padding: EdgeInsets.symmetric(
                                                  vertical:
                                                      getProportioanalScreenWidth(
                                                          10)),
                                              child: InkWell(
                                                onTap: () {},
                                                child: Text("Laporkan"),
                                              ))),
                                      Material(
                                          color: Colors.transparent,
                                          child: Ink(
                                              width: SizeConfig.screenWidth,
                                              height:
                                                  getProportioanalScreenWidth(
                                                      40),
                                              padding: EdgeInsets.symmetric(
                                                  vertical:
                                                      getProportioanalScreenWidth(
                                                          10)),
                                              child: InkWell(
                                                onTap: () {},
                                                child: Text(
                                                    "Beri Permintaan gabung grup"),
                                              ))),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Positioned(
                              top: 10,
                              left: 50,
                              right: 50,
                              child: Container(
                                width: 5,
                                height: getProportioanalScreenWidth(5),
                                margin: EdgeInsets.symmetric(
                                    horizontal:
                                        getProportioanalScreenWidth(110)),
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.blue.withOpacity(0.7),
                                ),
                              ))
                        ],
                      );
                    });
              },
            )
          ],
        ),
      ),
    );
  }
}
