part of template;

class DetailUser extends StatelessWidget {
  const DetailUser({Key key}) : super(key: key);
  static String routeName = "/detail_user";
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    final UserDetailArgument argument =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      key: _scaffoldKey,
      endDrawer: SafeArea(
        child: Container(
          width: SizeConfig.screenWidth * 0.7,
          color: Colors.white,
          child: Container(
            margin: EdgeInsets.only(
                left: getProportioanalScreenWidth(20),
                right: getProportioanalScreenWidth(20),
                top: getProportioanalScreenWidth(20)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${argument.userBaru.nickname}',
                    style:
                        TextStyle(fontSize: getProportioanalScreenWidth(20))),
                Divider(
                  height: 20,
                  color: kTextGrey,
                ),
                SizedBox(height: getProportioanalScreenWidth(10)),
                Row(
                  children: [
                    Icon(
                      Icons.bookmark_border,
                      size: 35,
                    ),
                    SizedBox(width: getProportioanalScreenWidth(10)),
                    Text("Disimpan",
                        style: TextStyle(
                            fontSize: getProportioanalScreenWidth(20)))
                  ],
                ),
                SizedBox(height: getProportioanalScreenWidth(10)),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushNamed(context, SideBarMenu.routeName),
                  child: Row(
                    children: [
                      Icon(Icons.more_horiz, size: 35),
                      SizedBox(width: getProportioanalScreenWidth(10)),
                      Text("Lainnya",
                          style: TextStyle(
                              fontSize: getProportioanalScreenWidth(20)))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      appBar: buildAppBarUser(
          context, argument, argument.userBaru.nickname, true, true, () {
        _scaffoldKey.currentState.openEndDrawer();
      }),
      body: DetailUserBody(
        userBaru: argument.userBaru,
      ),
    );
  }
}

class DetailUserBody extends StatefulWidget {
  const DetailUserBody({
    Key key,
    @required this.userBaru,
  }) : super(key: key);
  final UserBaru userBaru;

  @override
  _DetailUserBodyState createState() => _DetailUserBodyState();
}

class _DetailUserBodyState extends State<DetailUserBody> {
  int selectedIndex = 0;
  bool isFollow = false;
  int selectFollowIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: WillPopScope(
      onWillPop: () {
        setState(() {
          selectFollowIndex == 0
              ? Navigator.pop(context)
              : selectFollowIndex = 0;
        });
      },
      child: IndexedStack(
        index: selectFollowIndex,
        children: [
          Container(
            margin: EdgeInsets.only(top: getProportioanalScreenWidth(15)),
            child: SingleChildScrollView(
              child: Column(children: [
                buildRowProfileHeader(
                    widget.userBaru.imageUrl, widget.userBaru.name),
                SizedBox(
                  height: getProportionalScreenHeight(40),
                ),
                buildRowFollowCount(
                    "${widget.userBaru.posting}",
                    "${widget.userBaru.follower}",
                    "${widget.userBaru.following}"),
                SizedBox(
                  height: getProportionalScreenHeight(30),
                ),
                buildRowButton(),
                SizedBox(
                  height: getProportionalScreenHeight(20),
                ),
                // StoryDetail(
                //   widget: widget,
                //   fotoCover: widget.userBaru.fotoUrl[0],
                // ),
                buildPaddingTabbarMenu(),
                buildColumnActiveBar(),
                IndexedStack(
                  index: selectedIndex,
                  children: [getPost(), getPost(), getPost()],
                )
              ]),
            ),
          ),
          FindFollower(),
          FindFollowing(),
        ],
      ),
    ));
  }

  Row buildRowProfileHeader(String img, String textName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            CircleImageRound(
              imgUrl: img,
              radius: 50,
            ),
            SizedBox(
              height: getProportionalScreenHeight(10),
            ),
            Text(
              textName,
              style: TextStyle(
                  fontSize: getProportioanalScreenWidth(23),
                  fontWeight: FontWeight.w600),
            ),
          ],
        )
      ],
    );
  }

  Row buildRowFollowCount(
      String valuePost, String valueFollower, String valueFollowing) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  selectFollowIndex = 0;
                });
              },
              child: FollowerWidget(
                textValue: valuePost,
                title: "Postingan",
              ),
            ),
          ],
        ),
        SizedBox(
          width: getProportioanalScreenWidth(30),
        ),
        Column(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  selectFollowIndex = 1;
                });
              },
              child: FollowerWidget(
                // textValue: "${widget.userBaru.follower}",
                textValue: valueFollower,
                title: "Pengikut",
              ),
            ),
          ],
        ),
        SizedBox(
          width: getProportioanalScreenWidth(30),
        ),
        Column(
          children: [
            GestureDetector(
                onTap: () {
                  setState(() {
                    selectFollowIndex = 2;
                  });
                },
                child: FollowerWidget(
                  // textValue: "${widget.userBaru.following}",
                  textValue: valueFollowing,
                  title: "Mengikuti",
                )),
          ],
        ),
      ],
    );
  }

  Row buildRowButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: SizeConfig.screenWidth * 0.4,
          child: FlatButtonCustom(
            press: () {
              setState(() {
                isFollow = !isFollow;
              });
            },
            color: isFollow ? Colors.white : kPrimaryColor,
            shape: isFollow
                ? RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(width: 0.3))
                : RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
            text: isFollow ? "Mengikuti" : "Ikuti",
            colorText: isFollow ? Colors.black : Colors.white,
          ),
        ),
        SizedBox(
          width: getProportioanalScreenWidth(25),
        ),
        Container(
          width: SizeConfig.screenWidth * 0.4,
          child: OutlineButtonCustom(),
        )
      ],
    );
  }

  Padding buildPaddingTabbarMenu() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 0;
              });
            },
            color: selectedIndex == 0 ? kPrimaryColor : Colors.black,
            widthSize: 0.2,
            text: "Hafalan",
          ),
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 1;
              });
            },
            color: selectedIndex == 1 ? kPrimaryColor : Colors.black,
            widthSize: 0.4,
            text: "Foto/Vidio",
          ),
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 2;
              });
            },
            color: selectedIndex == 2 ? kPrimaryColor : Colors.black,
            widthSize: 0.2,
            text: "Ditandai",
          ),
        ],
      ),
    );
  }

  Column buildColumnActiveBar() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.3,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 0 ? kPrimaryColor : Colors.transparent),
            ),
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.4,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 1 ? kPrimaryColor : Colors.transparent),
            ),
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.3,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 2 ? kPrimaryColor : Colors.transparent),
            )
          ],
        ),
        Container(
          height: 0.5,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(color: Colors.black.withOpacity(0.8)),
        )
      ],
    );
  }

  Widget getPost() {
    return Container(
      margin: EdgeInsets.only(top: getProportioanalScreenWidth(3)),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: 3,
        runSpacing: 3,
        children: [
          ...List.generate(widget.userBaru.fotoUrl.length, (index) {
            return Container(
              height: 150,
              width: (SizeConfig.screenWidth - 6) / 3,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(widget.userBaru.fotoUrl[index]),
                      fit: BoxFit.cover)),
            );
          })
        ],
      ),
    );
  }
}

class UserDetailArgument {
  final UserBaru userBaru;

  UserDetailArgument({@required this.userBaru});
}
