part of template;

class LoginSuccessLayout extends StatelessWidget {
  const LoginSuccessLayout({Key key}) : super(key: key);
  static String routeName = "/login_succes";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          "Berhasil Masuk",
          style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600),
        ),
      ),
      body: BodySuccess(),
    );
  }
}

class BodySuccess extends StatefulWidget {
  const BodySuccess({Key key}) : super(key: key);

  @override
  _BodySuccessState createState() => _BodySuccessState();
}

class _BodySuccessState extends State<BodySuccess> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 4),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LandingPage())));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.screenHeight * 0.08,
          ),
          Image.asset("assets/images/login_success.png",
              height: SizeConfig.screenHeight * 0.4),
          SizedBox(
            height: SizeConfig.screenHeight * 0.08,
          ),
          Text("Berhasil Masuk",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "Balooda",
                  color: kPrimaryColor,
                  fontSize: getProportioanalScreenWidth(36))),
        ],
      ),
    );
  }
}
