part of template;

class LoginBodyHeader extends StatelessWidget {
  const LoginBodyHeader({Key key, this.content}) : super(key: key);
  final Widget content;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SizedBox(
      width: double.infinity,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: SizeConfig.screenHeight * 0.05,
            ),
            Text("Masuk ke MyVoQu",
                style: TextStyle(
                    fontFamily: "Balooda",
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: getProportioanalScreenWidth(28))),
            Text(
              "Masuk dengan email dan password \n atau dengan media sosial",
              style: GoogleFonts.poppins(
                  color: kTextGrey, fontSize: getProportioanalScreenWidth(14)),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.04,
            ),
            content,
            SizedBox(height: getProportionalScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SocialMediaCard(
                  icon: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocialMediaCard(
                  icon: "assets/icons/google.svg",
                  press: () {},
                ),
              ],
            ),
          ],
        ),
      ),
    ));
  }
}
