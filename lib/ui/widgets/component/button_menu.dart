part of template;

class MenuSidebar extends StatelessWidget {
  const MenuSidebar({
    Key key,
    this.icon,
    this.img,
    this.textMenu,
    this.press,
  }) : super(key: key);
  final Icon icon;
  final String img;
  final String textMenu;
  final Function press;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Row(
          children: [
            Container(
              width: SizeConfig.screenWidth * 0.23,
              height: SizeConfig.screenHeight * 0.13,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                  color: "F8F8FF".toColor(),
                  boxShadow: [
                    BoxShadow(
                        color: kTextGrey.withOpacity(0.38),
                        spreadRadius: 0.0,
                        blurRadius: 5,
                        offset: Offset(0, 5))
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [icon, Text(textMenu)],
              ),
            ),
            Container(
                width: SizeConfig.screenWidth * 0.23,
                height: SizeConfig.screenHeight * 0.13,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(img), fit: BoxFit.fill),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          color: kTextGrey.withOpacity(0.38),
                          spreadRadius: 0.0,
                          blurRadius: 5,
                          offset: Offset(0.0, 5))
                    ]))
          ],
        ),
      ),
    );
  }
}
