part of template;

class AppBarLanding extends StatelessWidget {
  const AppBarLanding({
    Key key,
    // this.icon,
    // this.daftar,
  }) : super(key: key);
  // final IconButton icon;
  // final String daftar;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: getProportionalScreenHeight(10)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "MyVoQu",
            style: TextStyle(
                fontFamily: 'Balooda',
                fontWeight: FontWeight.bold,
                fontSize: getProportioanalScreenWidth(34),
                color: kPrimaryColor),
          ),
        ],
      ),
    );
  }
}

class AppBarLogin extends StatelessWidget {
  const AppBarLogin({Key key, this.icon, this.text, this.press})
      : super(key: key);
  final IconButton icon;
  final String text;
  final Function press;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: getProportionalScreenHeight(10)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: [
              icon,
            ],
          ),
          Column(
            children: [
              Row(
                children: [
                  Row(
                    children: [
                      Text(
                        "MyVoQu",
                        style: TextStyle(
                            fontFamily: 'Balooda',
                            fontWeight: FontWeight.bold,
                            fontSize: getProportioanalScreenWidth(34),
                            color: kPrimaryColor),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      FlatButton(
                        onPressed: () {},
                        child: Text(
                          text,
                          style: GoogleFonts.poppins(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w400),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
