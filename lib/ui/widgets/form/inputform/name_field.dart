part of template;

class NameFormField extends StatelessWidget {
  const NameFormField({Key key, this.hint, this.label, this.icon})
      : super(key: key);
  final String hint;
  final String label;
  final String icon;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        keyboardType: TextInputType.name,
        decoration: InputDecoration(
            hintText: hint,
            labelText: label,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: CustomSuffixIcon(
              svgIcon: icon,
            )));
  }
}
