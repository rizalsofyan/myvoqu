part of template;

class GroupChattingLayout extends StatelessWidget {
  const GroupChattingLayout({Key key}) : super(key: key);
  static String routeName = "/group_chat";
  @override
  Widget build(BuildContext context) {
    final GroupChattingArgument value =
        ModalRoute.of(context).settings.arguments;
    List<GroupChattingModel> groupChatting = groupChattings;
    return Theme(
      data: ThemeData(inputDecorationTheme: null),
      child: Scaffold(
        appBar: AppBar(
          titleSpacing: 0,

          title: Container(
              child: Row(children: [
            Material(
              child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // borderRadius: BorderRadius.all(Radius.circular(100)),
                  image: value.groupList.imgGroup.isEmpty
                      ? DecorationImage(
                          image: AssetImage(
                              "assets/images/background_group_img.png"),
                          fit: BoxFit.fill,
                        )
                      : DecorationImage(
                          image: AssetImage(value.groupList.imgGroup),
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              shape: new CircleBorder(),
            ),
            SizedBox(
              width: getProportioanalScreenWidth(10),
            ),
            Flexible(
              child: Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${value.groupList.name}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: getProportioanalScreenWidth(5),
                      ),
                      Text(groupMembers.map((e) => e.user.name).toString(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    ]),
              ),
            )
          ])),
          // centerTitle: true,
          // bottom: PreferredSize(
          //     preferredSize: null,
          //     child: Flexible(
          //       child: Container(
          //           margin: EdgeInsets.symmetric(horizontal: 100, vertical: 5),
          //           child: Text(groupMembers.map((e) => e.user.name).toString(),
          //               maxLines: 1,
          //               overflow: TextOverflow.ellipsis,
          //               style: TextStyle(color: Colors.white))),
          //     )),
          actions: [
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.more_vert,
                    color: Colors.white, size: getProportioanalScreenWidth(35)))
          ],
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: getProportioanalScreenWidth(20)),
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          ...List.generate(groupChatting.length, (index) {
                            return Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  groupChatting[index].listFriend !=
                                          currentUser1
                                      ? Container(
                                          child: CircleAvatar(
                                            radius: 20,
                                            backgroundImage:
                                                CachedNetworkImageProvider(
                                                    groupChatting[index]
                                                        .listFriend
                                                        .fotoURl),
                                          ),
                                        )
                                      : SizedBox.shrink(),
                                  Container(
                                      width: groupChatting[index].listFriend ==
                                              currentUser1
                                          ? SizeConfig.screenWidth * 0.85
                                          : null,
                                      margin: EdgeInsets.only(
                                        left: getProportioanalScreenWidth(5),
                                        top: getProportioanalScreenWidth(20),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            groupChatting[index].listFriend !=
                                                    currentUser1
                                                ? CrossAxisAlignment.start
                                                : CrossAxisAlignment.end,
                                        children: [
                                          ...List.generate(
                                              groupChatting[index]
                                                  .chatText
                                                  .length, (indexchat) {
                                            return Container(
                                              padding: EdgeInsets.symmetric(
                                                horizontal:
                                                    getProportioanalScreenWidth(
                                                        20),
                                                vertical:
                                                    getProportioanalScreenWidth(
                                                        10),
                                              ),
                                              margin: EdgeInsets.symmetric(
                                                vertical:
                                                    getProportioanalScreenWidth(
                                                        1),
                                              ),
                                              decoration: BoxDecoration(
                                                  color: groupChatting[index]
                                                              .listFriend !=
                                                          currentUser1
                                                      ? Colors.black54
                                                      : kPrimaryColor,
                                                  borderRadius:
                                                      indexchat == 0 &&
                                                              groupChatting[index]
                                                                      .chatText
                                                                      .length !=
                                                                  1
                                                          ? BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(50),
                                                              topRight: Radius
                                                                  .circular(50),
                                                              bottomRight: groupChatting[
                                                                              index]
                                                                          .listFriend !=
                                                                      currentUser1
                                                                  ? Radius
                                                                      .circular(
                                                                          50)
                                                                  : Radius
                                                                      .circular(
                                                                          0),
                                                              bottomLeft: groupChatting[
                                                                              index]
                                                                          .listFriend !=
                                                                      currentUser1
                                                                  ? Radius
                                                                      .circular(
                                                                          0)
                                                                  : Radius
                                                                      .circular(
                                                                          50),
                                                            )
                                                          : indexchat ==
                                                                      groupChatting[index]
                                                                              .chatText
                                                                              .length -
                                                                          1 &&
                                                                  groupChatting[index]
                                                                          .chatText
                                                                          .length !=
                                                                      1
                                                              ? BorderRadius
                                                                  .only(
                                                                  topLeft: groupChatting[index]
                                                                              .listFriend !=
                                                                          currentUser1
                                                                      ? Radius
                                                                          .circular(
                                                                              0)
                                                                      : Radius
                                                                          .circular(
                                                                              50),
                                                                  topRight: groupChatting[index]
                                                                              .listFriend !=
                                                                          currentUser1
                                                                      ? Radius
                                                                          .circular(
                                                                              50)
                                                                      : Radius
                                                                          .circular(
                                                                              0),
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          50),
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          50),
                                                                )
                                                              : indexchat == 0 &&
                                                                      groupChatting[index]
                                                                              .chatText
                                                                              .length ==
                                                                          1
                                                                  ? BorderRadius
                                                                      .circular(
                                                                          50)
                                                                  : BorderRadius
                                                                      .only(
                                                                      topLeft: groupChatting[index].listFriend ==
                                                                              currentUser1
                                                                          ? Radius.circular(
                                                                              50)
                                                                          : Radius.circular(
                                                                              0),
                                                                      bottomLeft: groupChatting[index].listFriend ==
                                                                              currentUser1
                                                                          ? Radius.circular(
                                                                              50)
                                                                          : Radius.circular(
                                                                              0),
                                                                      topRight: groupChatting[index].listFriend !=
                                                                              currentUser1
                                                                          ? Radius.circular(
                                                                              50)
                                                                          : Radius.circular(
                                                                              0),
                                                                      bottomRight: groupChatting[index].listFriend !=
                                                                              currentUser1
                                                                          ? Radius.circular(
                                                                              50)
                                                                          : Radius.circular(
                                                                              0),
                                                                    )),
                                              child: Text(
                                                  groupChatting[index]
                                                      .chatText[indexchat],
                                                  maxLines: 1000,
                                                  style: TextStyle(
                                                      color: Colors.white)),
                                            );
                                          })
                                        ],
                                      )),
                                ],
                              ),
                            );
                          })
                        ],
                      )),
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: getProportioanalScreenWidth(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: "Tulis Pesan",
                            filled: true,
                            fillColor: Colors.grey[300],
                            // focusColor: Colors.grey[200],
                            // focusedBorder: InputBorder.none,

                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50),
                              borderSide: BorderSide.none,
                            ),
                            prefixIcon: IconButton(
                                icon: Icon(MdiIcons.emoticon, size: 30),
                                onPressed: null),
                          ),
                        ),
                      ),
                      Material(
                          child: Ink(
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: kPrimaryColor,
                              ),
                              child: InkWell(
                                customBorder: const CircleBorder(),
                                onTap: () {},
                                child: Icon(MdiIcons.microphone,
                                    color: Colors.white),
                              )))
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

class GroupChattingArgument {
  final GroupListModel groupList;

  GroupChattingArgument({@required this.groupList});
}
