part of template;

class CustomSuffixIcon extends StatelessWidget {
  const CustomSuffixIcon({
    Key key,
    this.svgIcon,
  }) : super(key: key);

  final String svgIcon;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, getProportioanalScreenWidth(20),
          getProportioanalScreenWidth(20), getProportioanalScreenWidth(20)),
      child: SvgPicture.asset(
        svgIcon,
        color: kTextGrey,
      ),
    );
  }
}
