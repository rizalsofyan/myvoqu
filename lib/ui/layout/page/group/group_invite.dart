part of template;

class GroupInviteNotification extends StatefulWidget {
  const GroupInviteNotification({Key key}) : super(key: key);
  static String routeName = "/group_invite";

  @override
  _GroupInviteNotificationState createState() =>
      _GroupInviteNotificationState();
}

class _GroupInviteNotificationState extends State<GroupInviteNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarGroup(context, "Undangan Grup", false, null, null),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: getProportioanalScreenWidth(10)),
                child: Column(
                  children: [
                    ...List.generate(groupInvites.length, (index) {
                      return Container(
                          child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                      width: getProportioanalScreenWidth(70),
                                      height: getProportioanalScreenWidth(70),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        // borderRadius: BorderRadius.all(Radius.circular(100)),
                                        image: groupInvites[index]
                                                .groupList
                                                .imgGroup
                                                .isEmpty
                                            ? DecorationImage(
                                                image: AssetImage(
                                                    "assets/images/background_group_img.png"),
                                                fit: BoxFit.fill,
                                              )
                                            : DecorationImage(
                                                image: AssetImage(
                                                    groupInvites[index]
                                                        .groupList
                                                        .imgGroup),
                                                fit: BoxFit.cover,
                                              ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: getProportioanalScreenWidth(10),
                                    ),
                                    Container(
                                      width: groupInvites[index]
                                                      .groupList
                                                      .isJoin ==
                                                  true ||
                                              groupInvites[index].isAccepted ==
                                                  true ||
                                              groupInviteStatus[index]
                                                      .statusConfirm !=
                                                  0
                                          ? SizeConfig.screenWidth * 0.5
                                          : SizeConfig.screenWidth * 0.25,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              groupInvites[index]
                                                  .groupList
                                                  .name
                                                  .toString(),
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                          Text(
                                              groupInvites[index]
                                                  .groupList
                                                  .createdBy
                                                  .toString(),
                                              style:
                                                  TextStyle(color: kTextGrey)),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              groupInvites[index].groupList.isJoin == true &&
                                          groupInvites[index].isAccepted ==
                                              true ||
                                      groupInviteStatus[index].statusConfirm ==
                                          1
                                  ? Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        border: Border.all(
                                            color: kPrimaryColor, width: 1),
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white,
                                      ),
                                      child: Text("Diterima",
                                          style:
                                              TextStyle(color: kPrimaryColor)))
                                  : groupInvites[index].groupList.isJoin == true &&
                                              groupInvites[index].isAccepted ==
                                                  false ||
                                          groupInviteStatus[index].statusConfirm ==
                                              3
                                      ? Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            border: Border.all(
                                                color: kTextGrey, width: 1),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: Colors.white,
                                          ),
                                          child: Text("Menunggu",
                                              style:
                                                  TextStyle(color: kTextGrey)))
                                      : groupInvites[index].groupList.isJoin ==
                                                      false &&
                                                  groupInvites[index].isAccepted ==
                                                      true ||
                                              groupInviteStatus[index].statusConfirm ==
                                                  2
                                          ? Container(
                                              padding: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                border: Border.all(
                                                    color: Colors.red,
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: Colors.white,
                                              ),
                                              child: Text("Ditolak",
                                                  style: TextStyle(color: Colors.red)))
                                          : groupInvites[index].groupList.isJoin == false && groupInvites[index].isAccepted == true || groupInviteStatus[index].statusConfirm == 5
                                              ? SizedBox.shrink()
                                              : Container(
                                                  child: Row(
                                                  children: [
                                                    FlatButtonCustom(
                                                      press: () {
                                                        setState(() {
                                                          groupInvites[index]
                                                                  .groupList
                                                                  .isJoin =
                                                              !groupInvites[
                                                                      index]
                                                                  .groupList
                                                                  .isJoin;
                                                        });
                                                      },
                                                      color: Colors.blue,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                      text: "Terima",
                                                      colorText: Colors.white,
                                                    ),
                                                    SizedBox(
                                                        width:
                                                            getProportioanalScreenWidth(
                                                                5)),
                                                    FlatButtonCustom(
                                                      press: () {
                                                        setState(() {
                                                          groupInviteStatus[
                                                                  index]
                                                              .statusConfirm = 5;
                                                          groupInvites[index]
                                                                  .groupList
                                                                  .isJoin =
                                                              !groupInvites[
                                                                      index]
                                                                  .groupList
                                                                  .isJoin;
                                                          groupInvites[index]
                                                                  .groupList
                                                                  .isJoin =
                                                              !groupInvites[
                                                                      index]
                                                                  .groupList
                                                                  .isJoin;
                                                        });
                                                      },
                                                      color: Colors.red,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                      text: "Tolak",
                                                      colorText: Colors.white,
                                                    )
                                                  ],
                                                )),
                            ],
                          ),
                        ],
                      ));
                    })
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
