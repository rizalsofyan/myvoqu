part of template;

class FindFollowing extends StatefulWidget {
  const FindFollowing({
    Key key,
  }) : super(key: key);

  @override
  _FindFollowingState createState() => _FindFollowingState();
}

class _FindFollowingState extends State<FindFollowing> {
  List<ListFriend> follower = listFriends;
  List<ListFriend> following = listFriends;
  String query = '';
  bool isFollow = false;
  int isSelected;
  int tabSelectedIndex = 1;

  @override
  void initState() {
    super.initState();

    follower = listFriends;
    following = listFriends;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          inputDecorationTheme: null,
        ),
        child: Container(
          child: Column(
            children: [
              buildRowTabbarMenu(),
              buildActiveBar(),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        IndexedStack(
                          index: tabSelectedIndex,
                          children: [
                            buildBodyFollowerList(context),
                            buildBodyFollowingList(context),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Row buildRowTabbarMenu() {
    return Row(children: [
      TabbarDetailCustom(
        press: () {
          setState(() {
            tabSelectedIndex = 0;
          });
        },
        text: "${listFriends.length} Pengikut",
        widthSize: 0.5,
        color: tabSelectedIndex == 0 ? kPrimaryColor : kTextGrey,
      ),
      TabbarDetailCustom(
        press: () {
          setState(() {
            tabSelectedIndex = 1;
          });
        },
        text: "${listFriends.length} Mengikuti",
        widthSize: 0.5,
        color: tabSelectedIndex == 1 ? kPrimaryColor : kTextGrey,
      ),
    ]);
  }

  Container buildBodyFollowingList(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
      child: Column(
        children: [
          Container(
            margin:
                EdgeInsets.symmetric(vertical: getProportioanalScreenWidth(20)),
            child: SearchWidget(
              hintText: "Cari",
              text: query,
              onChanged: searchFollowing,
            ),
          ),
          Container(
              margin: EdgeInsets.symmetric(
                  vertical: getProportioanalScreenWidth(5)),
              child: Column(
                children: [
                  ...List.generate(
                      (following.length) - 10,
                      (index) => InkWell(
                            onTap: () => Navigator.pushNamed(
                                context, DetailFollower.routeName,
                                arguments: FollowerDetailArgument(
                                    listFriend: follower[index])),
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: getProportioanalScreenWidth(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  buildContainerRowDataFollow(
                                      index,
                                      '${following[index].name}',
                                      '${following[index].nickname}',
                                      following[index].fotoURl),
                                  buildButtonFollow(index)
                                ],
                              ),
                            ),
                          ))
                ],
              ))
        ],
      ),
    );
  }

  Container buildBodyFollowerList(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
      child: Column(
        children: [
          Container(
            margin:
                EdgeInsets.symmetric(vertical: getProportioanalScreenWidth(20)),
            child: SearchWidget(
              hintText: "Cari",
              text: query,
              onChanged: searchFollower,
            ),
          ),
          Container(
              margin: EdgeInsets.symmetric(
                  vertical: getProportioanalScreenWidth(5)),
              child: Column(
                children: [
                  ...List.generate(
                      follower.length,
                      (index) => InkWell(
                            onTap: () => Navigator.pushNamed(
                                context, DetailFollower.routeName,
                                arguments: FollowerDetailArgument(
                                    listFriend: follower[index])),
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: getProportioanalScreenWidth(10)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  buildContainerRowDataFollow(
                                      index,
                                      '${follower[index].name}',
                                      '${follower[index].nickname}',
                                      follower[index].fotoURl),
                                  buildButtonFollow(index)
                                ],
                              ),
                            ),
                          ))
                ],
              ))
        ],
      ),
    );
  }

  Container buildContainerRowDataFollow(
      int index, String name, String nickname, String imgUrl) {
    return Container(
        child: Row(
      children: [
        CircleAvatar(
          radius: 25,
          backgroundImage: CachedNetworkImageProvider(imgUrl),
          backgroundColor: Colors.grey,
        ),
        SizedBox(
          width: getProportioanalScreenWidth(10),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(name, style: TextStyle(fontWeight: FontWeight.bold)),
            Text(nickname, style: TextStyle(color: kTextGrey)),
          ],
        ),
      ],
    ));
  }

  Container buildButtonFollow(int index) {
    return Container(
        child: FlatButtonCustom(
      press: () {
        setState(() {
          follower[index].isFollow = !follower[index].isFollow;
          // isFollow = follower[index].isFollow;
          // print(isSelected);
          // if (isSelected == index) {
          //   follower[index].isFollow=!follower[index].isFollow;
          // }
        });
      },
      color: follower[index].isFollow ? Colors.white : kPrimaryColor,
      shape: follower[index].isFollow
          ? RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(width: 0.3))
          : RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      text: follower[index].isFollow ? "Hapus" : "Ikuti",
      colorText: follower[index].isFollow ? Colors.black : Colors.white,
    ));
  }

  Column buildActiveBar() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 2,
              width: SizeConfig.screenWidth * 0.5,
              decoration: BoxDecoration(
                color:
                    tabSelectedIndex == 0 ? kPrimaryColor : Colors.transparent,
              ),
            ),
            Container(
              height: 2,
              width: SizeConfig.screenWidth * 0.5,
              decoration: BoxDecoration(
                color: tabSelectedIndex == 1 ? kPrimaryColor : Colors.grey[300],
              ),
            ),
          ],
        ),
        Container(
          height: 2,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(color: Colors.grey[300].withOpacity(0.2)),
        )
      ],
    );
  }

  void searchFollower(String query) {
    final followers = listFriends.where((element) {
      return element.name.toLowerCase().contains(query.toLowerCase()) ||
          element.nickname.toLowerCase().contains(query.toLowerCase());
    }).toList();
    setState(() {
      this.query = query;
      this.follower = followers;
    });
  }

  void searchFollowing(String query) {
    final followings = listFriends.where((element) {
      return element.name.toLowerCase().contains(query.toLowerCase()) ||
          element.nickname.toLowerCase().contains(query.toLowerCase());
    }).toList();
    setState(() {
      this.query = query;
      this.following = followings;
    });
  }
}
