part of template;

class PasswordFormField extends StatelessWidget {
  const PasswordFormField({Key key, this.secureText, this.hint, this.label})
      : super(key: key);
  final bool secureText;
  final String hint;
  final String label;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        obscureText: secureText,
        decoration: InputDecoration(
            hintText: hint,
            labelText: label,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: CustomSuffixIcon(
              svgIcon: "assets/icons/lock-on.svg",
            )));
  }
}
