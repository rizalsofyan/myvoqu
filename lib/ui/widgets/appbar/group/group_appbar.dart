part of template;

AppBar appBarGroup(BuildContext context, String title, bool showAction,
    Function pressNotif, Function more) {
  return AppBar(
    title: Text(title),
    elevation: 0,
    actions: [
      showAction == true
          ? IconButton(
              icon: Icon(Icons.notifications, color: Colors.white),
              onPressed: pressNotif)
          : SizedBox.shrink(),
      showAction == true
          ? IconButton(
              icon: Icon(Icons.more_vert, color: Colors.white), onPressed: more)
          : SizedBox.shrink(),
    ],
  );
}
