part of template;

class LandingBody extends StatefulWidget {
  @override
  _LandingBodyState createState() => _LandingBodyState();
}

class _LandingBodyState extends State<LandingBody> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: SizeConfig.screenHeight * 0.2,
              ),
              AppBarLanding(),
              SizedBox(
                height: SizeConfig.screenHeight * 0.3,
              ),
              Text(
                "Menghafal Alquran Semudah Tersenyum \nDengan Gerakan Dan Video.",
                style: GoogleFonts.poppins(
                    color: Colors.black,
                    fontSize: getProportioanalScreenWidth(16)),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: getProportionalScreenHeight(20),
              ),
              // Spacer(),
              DefaultButton(
                press: () {
                  Navigator.pushNamed(context, RegisterPage.routeName);
                },
                text: "Buat Akun",
              ),
              // Expanded(
              //     flex: 4,
              //     child: SizedBox(
              //       child: Column(
              //         children: <Widget>[
              //           // Spacer(),

              //         ],
              //       ),
              //     )),
              // SizedBox(
              //   height: getProportionalScreenHeight(20),
              // ),
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: getProportioanalScreenWidth(20)),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Sudah Punya Akun ? ",
                                style: GoogleFonts.poppins(
                                    fontSize: getProportioanalScreenWidth(16))),
                            GestureDetector(
                              onTap: () => Navigator.pushNamed(
                                  context, LoginPage.routeName),
                              child: Text(
                                "Masuk",
                                style: GoogleFonts.poppins(
                                    color: kPrimaryColor,
                                    fontSize: getProportioanalScreenWidth(16)),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
