part of template;

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LandingPage())));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.3,
                  ),
                  Text(
                    "MyVoQu",
                    style: TextStyle(
                        fontSize: getProportioanalScreenWidth(50),
                        color: Colors.white,
                        fontFamily: 'Balooda',
                        fontWeight: FontWeight.w800),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.2,
                  ),
                  Text(
                    "Menghafal Semudah Tersenyum",
                    style: GoogleFonts.poppins(color: Colors.white),
                  ),
                  SvgPicture.asset(
                    "assets/icons/smile.svg",
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
