part of template;

class GroupTaskLayout extends StatelessWidget {
  const GroupTaskLayout({Key key}) : super(key: key);
  static String routeName = "/group_task";
  @override
  Widget build(BuildContext context) {
    List<GroupTaskModel> groupTask = groupTasks;
    return Scaffold(
        appBar: appBarGroup(context, "Tugas Grup", false, null, null),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: getProportioanalScreenWidth(20)),
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ...List.generate(groupTask.length, (index) {
                      return Container(
                          child: Column(
                        children: [
                          Container(
                              child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: getProportioanalScreenWidth(20),
                                    vertical: getProportioanalScreenWidth(10)),
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                        child: Row(
                                      children: [
                                        Container(
                                            width: 50,
                                            height: 50,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                              color:
                                                  Colors.grey.withOpacity(0.8),
                                            ),
                                            child: groupTask[index].imgTask !=
                                                    null
                                                ? CircleAvatar(
                                                    radius: 50,
                                                    backgroundImage:
                                                        CachedNetworkImageProvider(
                                                      groupTask[index].imgTask,
                                                    ),
                                                  )
                                                : Icon(MdiIcons.account,
                                                    color: Colors.black
                                                        .withOpacity(0.8))),
                                        Flexible(
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                left:
                                                    getProportioanalScreenWidth(
                                                        10)),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                    "${groupTask[index].titleTask} ${groupTask[index].subtitleTask}",
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize:
                                                            getProportioanalScreenWidth(
                                                                18),
                                                        fontWeight:
                                                            FontWeight.w600)),
                                                Text(
                                                    "${groupTask[index].dateTaskPost} pada ${groupTask[index].timeTaskPost}",
                                                    style: TextStyle(
                                                        fontSize:
                                                            getProportioanalScreenWidth(
                                                                12),
                                                        color:
                                                            Colors.grey[700])),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: getProportioanalScreenWidth(20),
                                  vertical: getProportioanalScreenWidth(10),
                                ),
                                child: ExpandableText(
                                  "${groupTask[index].captionTask}",
                                  style: TextStyle(
                                      letterSpacing: -0.5,
                                      fontSize: 14,
                                      wordSpacing: 0.5),
                                  expandText: "Lainnya",
                                  collapseText: "Sedikit",
                                  linkColor: kTextGrey,
                                  maxLines: 2,
                                ),
                              ),
                              Stack(
                                children: [
                                  Container(
                                    width: SizeConfig.screenWidth,
                                    height: SizeConfig.screenHeight * 0.5,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            fit: BoxFit.fill,
                                            image: CachedNetworkImageProvider(
                                                groupTask[index].fileTask))),
                                  ),
                                  Positioned(
                                      top: 5,
                                      right: 5,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal:
                                                getProportioanalScreenWidth(20),
                                            vertical:
                                                getProportioanalScreenWidth(
                                                    10)),
                                        decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius:
                                              BorderRadiusDirectional.circular(
                                                  5),
                                        ),
                                        child: Text(
                                          "Belum Mengumpulkan",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )),
                                  Positioned(
                                      top: 5,
                                      left: 5,
                                      child: Container(
                                          child: Text("00:31",
                                              style: TextStyle(
                                                  color: Colors.white)))),
                                  Positioned(
                                      top: 50,
                                      bottom: 50,
                                      right: 50,
                                      left: 50,
                                      child: Container(
                                          child: Icon(MdiIcons.playCircle,
                                              color: Colors.white,
                                              size: getProportioanalScreenWidth(
                                                  100))))
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: getProportioanalScreenWidth(20),
                                  vertical: getProportioanalScreenWidth(10),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/love_icon.svg",
                                          width: 27,
                                          color: Colors.black,
                                        ),
                                        SizedBox(
                                          width:
                                              getProportioanalScreenWidth(10),
                                        ),
                                        SvgPicture.asset(
                                          "assets/icons/comment_icon.svg",
                                          color: Colors.black,
                                          width: 27,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                          SizedBox(
                            height: getProportionalScreenHeight(10),
                          ),
                          Divider(
                            color: Colors.grey.withOpacity(0.5),
                            height: 2,
                          ),
                          SizedBox(
                            height: getProportionalScreenHeight(20),
                          ),
                        ],
                      ));
                    })
                  ],
                ))
          ],
        )));
  }
}
