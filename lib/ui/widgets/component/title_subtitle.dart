part of template;

class TItleSubtitleHeader extends StatelessWidget {
  const TItleSubtitleHeader({
    Key key,
    @required this.title,
    @required this.subtitle,
    this.sizeTitle,
    this.sizeSubtitle,
    this.colorSubtitle,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final double sizeTitle;
  final double sizeSubtitle;
  final Color colorSubtitle;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title,
          // currentUser.name,
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: sizeTitle),
        ),
        Text(
          subtitle,
          // "Lihat profil anda",
          style: TextStyle(color: colorSubtitle, fontSize: sizeSubtitle),
        )
      ],
    );
  }
}
