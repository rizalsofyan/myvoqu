import 'package:meta/meta.dart';
import 'package:quranku/models/models.dart';

class GroupListModel {
  final String name;
  final String createdBy;
  final String forGender;
  final double rateGroup;
  final int numberMemberCount;
  final String imgGroup;
  bool isJoin;

  GroupListModel(
      {@required this.name,
      @required this.createdBy,
      @required this.forGender,
      @required this.rateGroup,
      @required this.numberMemberCount,
      @required this.imgGroup,
      @required this.isJoin});
}

class GroupInfoModel {
  final User user;
  final String createdInfoDate;
  final String createInfoTime;
  final String infoPost;
  bool isLike;

  GroupInfoModel({
    @required this.user,
    @required this.createdInfoDate,
    @required this.createInfoTime,
    @required this.infoPost,
    @required this.isLike,
  });
}

class GroupMemberModel {
  final ListFriend user;
  final String roleMember;

  GroupMemberModel({this.user, @required this.roleMember});
}

class GrupDetailModel {
  final GroupListModel groupListModel;

  final List<GroupPostModel> groupPostModel;
  final User user;
  final List<GroupInfoModel> groupInfoModel;
  final List<GroupTaskModel> groupTaskModel;
  final List<GroupMemberModel> groupMemberModel;

  GrupDetailModel({
    @required this.groupListModel,
    @required this.groupPostModel,
    @required this.user,
    @required this.groupInfoModel,
    @required this.groupTaskModel,
    @required this.groupMemberModel,
  });
}

class GroupPostModel {
  final ListFriend user;
  final String lokasi;
  final String country;
  final String caption;
  final String timeAgo;
  final String fileUrl;

  final List<ListFriend> likesBy;
  final int comments;
  final List<CommentPost> commentPost;
  bool isLiked;

  GroupPostModel(
      {@required this.user,
      @required this.lokasi,
      @required this.country,
      @required this.caption,
      @required this.timeAgo,
      @required this.fileUrl,
      @required this.likesBy,
      @required this.comments,
      @required this.commentPost,
      @required this.isLiked});
}

class GroupTaskModel {
  final String imgTask;
  final String titleTask;
  final String subtitleTask;
  final String dateTaskPost;
  final String timeTaskPost;
  final String captionTask;
  final String fileTask;
  bool isLiked;
  final List<CommentPost> commentPostTask;

  GroupTaskModel(
      {@required this.imgTask,
      @required this.titleTask,
      @required this.subtitleTask,
      @required this.dateTaskPost,
      @required this.timeTaskPost,
      @required this.captionTask,
      @required this.fileTask,
      @required this.commentPostTask,
      @required this.isLiked});
}

class GroupInviteModel {
  final GroupListModel groupList;

  bool isAccepted;

  GroupInviteModel({
    @required this.groupList,
    @required this.isAccepted,
  });
}

class GroupInviteStatusModel {
  final GroupInviteModel groupInvite;
  int statusConfirm;

  GroupInviteStatusModel(
      {@required this.groupInvite, @required this.statusConfirm});
}

class GroupChattingModel {
  final ListFriend listFriend;
  final List<String> chatText;

  final int indexSend;

  GroupChattingModel(
      {@required this.listFriend,
      @required this.chatText,
      @required this.indexSend});
}
