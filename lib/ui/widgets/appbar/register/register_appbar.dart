part of template;

AppBar buildAppBarRegister(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    leading: IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pop(context);
      },
      color: kPrimaryColor,
    ),
    centerTitle: true,
    elevation: 0,
    title: Text(
      "MyVoQu",
      style: TextStyle(
          fontFamily: 'Balooda',
          fontWeight: FontWeight.bold,
          fontSize: getProportioanalScreenWidth(34),
          color: kPrimaryColor),
    ),
  );
}
