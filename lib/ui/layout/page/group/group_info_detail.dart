part of template;

class GroupInfoDetail extends StatefulWidget {
  const GroupInfoDetail({Key key}) : super(key: key);
  static String routeName = "/group_info";

  @override
  _GroupInfoDetailState createState() => _GroupInfoDetailState();
}

class _GroupInfoDetailState extends State<GroupInfoDetail> {
  List<GroupInfoModel> groupInfo = groupInfos;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarGroup(context, "Informasi", false, null, null),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: InkWell(
          onTap: () {},
          child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                blurRadius: 4,
                spreadRadius: 0.1,
                color: Colors.grey[400].withOpacity(0.7),
                offset: Offset(0, 4),
              )
            ]),
            child: Icon(MdiIcons.plus, size: 50, color: kPrimaryColor),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    ...List.generate(groupInfo.length, (index) {
                      return Container(
                          margin: EdgeInsets.only(
                              left: getProportioanalScreenWidth(20),
                              right: getProportioanalScreenWidth(20),
                              top: getProportioanalScreenWidth(20)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(
                                      bottom: getProportioanalScreenWidth(15)),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 50,
                                        height: 50,
                                        child: CircleAvatar(
                                          radius: 50,
                                          backgroundImage:
                                              CachedNetworkImageProvider(
                                                  groupInfos[index]
                                                      .user
                                                      .imageUrl),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: getProportioanalScreenWidth(
                                                10)),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 20,
                                              child: Text(
                                                  "${groupInfos[index].user.name}",
                                                  style: TextStyle(
                                                      fontSize:
                                                          getProportioanalScreenWidth(
                                                              18),
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ),
                                            SizedBox(
                                              height: 15,
                                              child: Text(
                                                  "${groupInfos[index].createdInfoDate} pada ${groupInfos[0].createInfoTime}",
                                                  style: TextStyle(
                                                      fontSize:
                                                          getProportioanalScreenWidth(
                                                              12),
                                                      color: Colors.grey[700])),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  )),
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: getProportioanalScreenWidth(15)),
                                child: ExpandableText(
                                  "${groupInfo[0].infoPost}",
                                  style: TextStyle(
                                      letterSpacing: -0.5,
                                      fontSize: 14,
                                      wordSpacing: 0.5),
                                  expandText: "Lainnya",
                                  collapseText: "Sedikit",
                                  linkColor: kTextGrey,
                                  maxLines: 2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    bottom: getProportioanalScreenWidth(15)),
                                child: SvgPicture.asset(
                                  "assets/icons/love_icon.svg",
                                  width: 25,
                                  color: Colors.black,
                                ),
                              ),
                              Divider(
                                height: 2,
                                color: Colors.grey.withOpacity(0.2),
                              )
                            ],
                          ));
                    })
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
