part of template;

class FormErorr extends StatelessWidget {
  const FormErorr({
    Key key,
    @required this.errors,
  }) : super(key: key);

  final List<String> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length, (index) => errorFormField(error: errors[index])),
    );
  }

  Row errorFormField({String error}) {
    return Row(
      children: [
        SvgPicture.asset(
          "assets/icons/error.svg",
          color: Colors.red,
          height: getProportioanalScreenWidth(14),
          width: getProportioanalScreenWidth(14),
        ),
        SizedBox(
          width: getProportioanalScreenWidth(10),
        ),
        Text(
          error,
          style: TextStyle(
              color: kTextGrey, fontSize: getProportioanalScreenWidth(10)),
        )
      ],
    );
  }
}
