part of template;

class SearchPage extends StatefulWidget {
  const SearchPage({
    Key key,
  }) : super(key: key);
  static String routeName = "/search";

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<UserBaru> friend = userBaru;
  String query = '';
  String error;
  @override
  void initState() {
    super.initState();
    friend = userBaru;
  }

  @override
  Widget build(BuildContext context) {
    this.query = query;
    return Theme(
        data: ThemeData(
          inputDecorationTheme: null,
        ),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              backgroundColor: Colors.white,
              elevation: 0,
              toolbarHeight: getProportionalScreenHeight(100),
              centerTitle: true,
              title: SizedBox(
                  width: SizeConfig.screenWidth,
                  height: 40,
                  child: SearchWidget(
                    hintText: "Cari Teman",
                    text: query,
                    onChanged: searchFriend,
                  ))),
          body: query.isNotEmpty
              ? SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    padding:
                        EdgeInsets.only(top: getProportionalScreenHeight(20)),
                    child: Column(
                      children: [
                        ...List.generate(
                            friend.length,
                            (index) => ListUser(
                                  friend: friend[index],
                                  press: () => Navigator.pushNamed(
                                      context, DetailUser.routeName,
                                      arguments: UserDetailArgument(
                                          userBaru: friend[index])),
                                )),
                      ],
                    ),
                  ),
                )
              : null,
        ));
  }

  void searchFriend(String query) {
    final friend = userBaru.where((friends) {
      final names = friends.name.toLowerCase();
      final nicknames = friends.nickname.toLowerCase();
      final searchLower = query.toLowerCase();

      return names.contains(searchLower) || nicknames.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.error = error;
      this.friend = friend;
    });
  }
}

class ListUser extends StatelessWidget {
  const ListUser({
    Key key,
    @required this.friend,
    @required this.press,
  }) : super(key: key);

  final UserBaru friend;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      hoverColor: Colors.grey,
      onTap: press,
      child: SizedBox(
        width: SizeConfig.screenWidth,
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                  vertical: getProportionalScreenHeight(15),
                  horizontal: getProportioanalScreenWidth(20)),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 25,
                        backgroundImage:
                            CachedNetworkImageProvider(friend.imageUrl),
                        backgroundColor: Colors.grey,
                      ),
                      SizedBox(
                        width: getProportioanalScreenWidth(10),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${friend.name}',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${friend.nickname}',
                              style: TextStyle(color: kTextGrey)),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SearchWidget extends StatefulWidget {
  const SearchWidget({Key key, this.text, this.onChanged, this.hintText})
      : super(key: key);
  final String text;
  final ValueChanged<String> onChanged;
  final String hintText;
  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return TextField(
        controller: controller,
        decoration: InputDecoration(
            hintText: widget.hintText,
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            suffixIcon: widget.text.isNotEmpty
                ? GestureDetector(
                    child: Icon(
                      Icons.close,
                      color: Colors.black,
                      size: getProportioanalScreenWidth(20),
                    ),
                    onTap: () {
                      controller.clear();
                      widget.onChanged('');
                      FocusScope.of(context).requestFocus(FocusNode());
                    },
                  )
                : null,
            filled: true,
            fillColor: Colors.grey[200],
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15),
                gapPadding: 10)),
        onChanged: widget.onChanged);
  }
}
