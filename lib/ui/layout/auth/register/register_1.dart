part of template;

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key key}) : super(key: key);
  static String routeName = "/register_page";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: buildAppBarRegister(context),
      body: RegisterBodyHeader(
        imgActive: "assets/images/img_active1.png",
        content: RegisterFormNama(),
      ),
    );
  }
}
