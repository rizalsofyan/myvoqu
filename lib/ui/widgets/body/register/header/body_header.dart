part of template;

class RegisterBodyHeader extends StatelessWidget {
  const RegisterBodyHeader({Key key, this.imgActive, this.content})
      : super(key: key);
  final String imgActive;
  final Widget content;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
          child: Column(
            children: [
              SizedBox(
                height: SizeConfig.screenHeight * 0.01,
              ),
              Text("Buat Akun", style: headingRegister),
              Text("Silahkan isi formulir data diri anda",
                  style: TextStyle(color: kTextGrey)),
              SizedBox(
                height: SizeConfig.screenHeight * 0.04,
              ),
              Image.asset(
                imgActive,
                width: SizeConfig.screenWidth * 0.6,
              ),
              content,
            ],
          ),
        ),
      ),
    );
  }
}
