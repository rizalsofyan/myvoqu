import 'package:meta/meta.dart';

class User {
  final String name;
  final String imageUrl;

  const User({@required this.name, @required this.imageUrl});

  factory User.fromJson(Map<String, dynamic> json) => User(
        name: json['name'],
        imageUrl: json['imageUrl'],
      );
}

class UserBaru {
  final String name;
  final String nickname;
  final String imageUrl;
  final int posting;
  final int follower;
  final int following;
  final List<String> fotoUrl;
  final String videoUrl;

  final String audioUrl;

  UserBaru(
      {@required this.name,
      @required this.nickname,
      @required this.imageUrl,
      @required this.posting,
      @required this.follower,
      @required this.following,
      @required this.fotoUrl,
      @required this.videoUrl,
      @required this.audioUrl});
}

class RandomUser {
  final User user;
  bool isFollow;

  RandomUser({@required this.user, @required this.isFollow});
}

class DetailNewUser {
  final UserBaru userBaru;
  final int posting;
  final int follower;
  final int following;
  final String fotoUrl;
  final String videoUrl;
  final String audioUrl;

  DetailNewUser(
      {@required this.userBaru,
      @required this.posting,
      @required this.follower,
      @required this.following,
      @required this.fotoUrl,
      @required this.videoUrl,
      @required this.audioUrl});
}

class ListFriend {
  final String name;
  final String nickname;
  final String fotoURl;
  final int follower;
  final int following;
  final ListHafalan listHafalan;
  final List<String> foto;
  final List<String> vidio;
  final List<String> ditandai;
  bool isFollow;

  ListFriend(
      {@required this.follower,
      @required this.following,
      @required this.listHafalan,
      @required this.name,
      @required this.nickname,
      @required this.fotoURl,
      @required this.foto,
      @required this.vidio,
      @required this.ditandai,
      @required this.isFollow});
}

class ListHafalan {
  final List<String> coverFoto;
  final int mulai;
  final int akhir;
  final String audio;

  ListHafalan(
      {@required this.coverFoto,
      @required this.mulai,
      @required this.akhir,
      @required this.audio});
}
