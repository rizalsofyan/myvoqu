part of template;

class SideBarMenu extends StatelessWidget {
  const SideBarMenu({Key key, @required this.currentUser}) : super(key: key);
  final User currentUser;
  static String routeName = "/sidebar";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.close,
              size: 27,
            ),
            onPressed: () => Navigator.pop(context)),
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
        elevation: 0,
        title: Text("Menu",
            style: TextStyle(
                color: Colors.black,
                fontSize: 24,
                fontWeight: FontWeight.w500)),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        child: Column(
          children: [
            Row(children: [
              CircleImageRound(
                imgUrl: currentUser.imageUrl,
                radius: 30,
              ),
              SizedBox(
                width: getProportioanalScreenWidth(10),
              ),
              TItleSubtitleHeader(
                title: currentUser.name,
                subtitle: "Lihat profil anda",
                colorSubtitle: kTextGrey,
                sizeSubtitle: 12,
              ),
              SizedBox(
                width: getProportioanalScreenWidth(50),
              ),
              Column(
                children: [
                  FollowerWidget(
                    textValue: "10Rb",
                    title: "Pengikut",
                  )
                ],
              ),
              SizedBox(
                width: getProportioanalScreenWidth(20),
              ),
              Column(
                children: [
                  FollowerWidget(
                    textValue: "100",
                    title: 'Mengikuti',
                  )
                ],
              ),
            ]),
            Divider(
              color: Colors.grey,
              height: getProportionalScreenHeight(40),
              thickness: 0.5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MenuSidebar(
                  icon: Icon(Icons.home),
                  textMenu: "Beranda",
                  img: "assets/images/beranda_img.png",
                  press: () => Navigator.pushNamed(context, HomePage.routeName),
                ),
                MenuSidebar(
                  icon: Icon(Icons.group),
                  textMenu: "Group",
                  img: "assets/images/group_img.png",
                  press: () => selectedItem(context, 1),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MenuSidebar(
                  icon: Icon(Icons.person_pin_circle),
                  textMenu: "Cari Teman",
                  img: "assets/images/find_img.png",
                  press: () => selectedItem(
                    context,
                    2,
                  ),
                ),
                MenuSidebar(
                  icon: Icon(Icons.power_settings_new),
                  textMenu: "Keluar",
                  img: "assets/images/exit_img.png",
                  press: () => selectedItem(context, 3),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

void selectedItem(BuildContext context, int index) {
  switch (index) {
    case 1:
      Navigator.pushNamed(context, GroupPage.routeName);
      break;
    case 2:
      Navigator.pushNamed(context, SearchPage.routeName);
      break;
    case 3:
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              content: Text(
                "Anda yakin ingin keluar?",
                textAlign: TextAlign.center,
              ),
              actions: [
                Container(
                  width: SizeConfig.screenWidth,
                  child: Column(
                    children: [
                      FlatButton(
                          color: "F63569".toColor(),
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.screenWidth * 0.23,
                              vertical: 20),
                          child: Text("Ya"),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          onPressed: () {}),
                      SizedBox(
                        height: getProportionalScreenHeight(20),
                      ),
                      OutlineButton(
                        child: Text(
                          "Tidak",
                          style: TextStyle(color: Colors.black),
                        ),
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.screenWidth * 0.20,
                            vertical: 18),
                        borderSide: BorderSide(width: 1, color: kPrimaryColor),
                        onPressed: () {},
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      SizedBox(
                        height: getProportionalScreenHeight(40),
                      ),
                    ],
                  ),
                )
              ],
            );
          });
      break;
    default:
  }
}
