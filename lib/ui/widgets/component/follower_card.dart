part of template;

class FollowerWidget extends StatelessWidget {
  const FollowerWidget({
    Key key,
    this.textValue,
    this.title,
  }) : super(key: key);
  final String textValue;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          textValue,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}
