part of template;

AppBar buildAppBarUser(BuildContext context, Object argument, String nickname,
    bool leadingBool, bool actionButton, Function press) {
  return AppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: leadingBool == true ? true : false,
      leading: leadingBool == true
          ? SizedBox(
              width: getProportioanalScreenWidth(40),
              height: getProportioanalScreenWidth(40),
              child: FlatButton(
                  color: Colors.grey[100],
                  padding: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back_ios)),
            )
          : null,
      elevation: 0,
      actions: [
        actionButton == true
            ? SizedBox(
                width: getProportioanalScreenWidth(70),
                height: getProportioanalScreenWidth(40),
                child: FlatButton(
                    color: Colors.grey[100],
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    child: Icon(
                      Icons.more_vert,
                      color: Colors.black,
                    ),
                    onPressed: press),
              )
            : SizedBox.shrink(),
      ],
      title: Text(
        nickname,
        style: TextStyle(color: Colors.black),
      ));
}
