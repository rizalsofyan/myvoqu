part of template;

class LandingPage extends StatelessWidget {
  static String routeName = "/landing";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: LandingBody(),
    );
  }
}
