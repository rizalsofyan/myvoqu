import 'package:meta/meta.dart';

import 'models.dart';

class Post {
  final User user;
  final String lokasi;
  final String caption;
  final String timeAgo;
  final String imgUrl;
  final int likes;
  final int comments;
  final int share;
  final List<GroupInfoModel> grupInfo;

  const Post(
      {@required this.user,
      @required this.lokasi,
      @required this.caption,
      @required this.timeAgo,
      @required this.imgUrl,
      @required this.likes,
      @required this.comments,
      @required this.share,
      @required this.grupInfo});
}
