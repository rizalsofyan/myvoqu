part of template;

class StoryDetail extends StatelessWidget {
  const StoryDetail({
    Key key,
    @required this.widget,
    this.fotoCover,
  }) : super(key: key);

  final DetailUserBody widget;
  final String fotoCover;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
      child: Row(
        children: [
          Container(
            width: getProportioanalScreenWidth(70),
            height: getProportioanalScreenWidth(70),
            // padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(width: 2, color: kPrimaryColor),
                color: Colors.white),
            child: Icon(
              Icons.add,
              size: 30,
              color: kPrimaryColor,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: getProportioanalScreenWidth(20)),
            width: getProportioanalScreenWidth(70),
            height: getProportioanalScreenWidth(70),
            // padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                border: Border.all(width: 2, color: kPrimaryColor),
                color: Colors.white),
            child: CircleAvatar(
              backgroundImage: CachedNetworkImageProvider(fotoCover),
            ),
          ),
        ],
      ),
    );
  }
}
