part of template;

class TermConditionPage extends StatefulWidget {
  const TermConditionPage({Key key}) : super(key: key);
  static String routeName = "/term_condition";

  @override
  _TermConditionPageState createState() => _TermConditionPageState();
}

class _TermConditionPageState extends State<TermConditionPage> {
  bool agree = false;

  void _doChecklist() {
    Navigator.pushNamed(context, RegisterSuccessLayout.routeName);
  }

  // void _onLoading() {
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return Dialog(
  //           child: new Row(
  //             mainAxisSize: MainAxisSize.min,
  //             children: [
  //               new CircularProgressIndicator(),
  //               new Text("Loading"),
  //             ],
  //           ),
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: buildAppBarRegister(context),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: getProportionalScreenHeight(20),
              ),
              Text(
                "Syarat dan Ketentuan",
                style: TextStyle(
                    fontSize: getProportioanalScreenWidth(22),
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: getProportionalScreenHeight(20),
              ),
              Text(
                "Dengan mengakses aplikasi ini, kami menganggap bahwa anda menerima syarat dan ketentuan ini secara penuh. Jangan terus menggunakan aplikasi MyVoQu jika anda tidak menerima syarat dan ketentuan yang tercantum pada halaman ini. ",
                style: TextStyle(color: kTextGrey),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: getProportionalScreenHeight(10),
              ),
              buildCheckboxTerm()
            ],
          ),
        ),
      ),
    );
  }

  SizedBox buildCheckboxTerm() {
    // bool onLoading = false;
    return SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Saya Setuju.",
                  style: TextStyle(color: kTextGrey),
                ),
                Checkbox(
                  value: agree,
                  onChanged: (value) {
                    setState(() {
                      agree = value ? true : false;
                    });
                  },
                  activeColor: kPrimaryColor,
                ),
              ],
            ),
            Text.rich(
              TextSpan(
                  text:
                      "Untuk rincian selengkapnya tentang pengaturan ini, kunjungi: ",
                  style: TextStyle(color: kTextGrey),
                  children: [
                    TextSpan(
                        text: "Pusat Bantuan",
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            //page pusat bantuan
                          },
                        style: TextStyle(
                            color: kPrimaryColor, fontWeight: FontWeight.bold))
                  ]),
            ),
            SizedBox(
              height: getProportionalScreenHeight(40),
            ),
            DefaultButton(
              press: () {
                agree
                    ? _doChecklist()
                    : showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Proses Tidak Dilanjutkan."),
                            content: Text("Harap setujui."),
                            actions: [
                              FlatButton(
                                  child: Text("OK"),
                                  onPressed: () => Navigator.of(context).pop()),
                            ],
                          );
                        });
              },
              text: "Lanjut",
            )
          ],
        ));
  }
}
