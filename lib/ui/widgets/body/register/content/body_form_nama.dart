part of template;

class RegisterFormNama extends StatefulWidget {
  const RegisterFormNama({Key key}) : super(key: key);

  @override
  _RegisterFormNamaState createState() => _RegisterFormNamaState();
}

class _RegisterFormNamaState extends State<RegisterFormNama> {
  String nama;
  String email;
  final List<String> errors = [];
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1900), //required
      lastDate: selectedDate, //required
      initialEntryMode: DatePickerEntryMode.input,
      helpText: 'Pilih Tanggal Lahir Anda',
      cancelText: 'Batalkan',
      confirmText: 'Lanjutkan',
      fieldLabelText: 'Tanggal Lahir',
      fieldHintText: 'Bulan/Tanggal/Tahun',

      errorFormatText: 'Eror!! Tanggal Tidak Valid',
      errorInvalidText: 'Eror!! Tanggal tidak dalam ukuran',
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            NameFormField(
              hint: "Nama Lengkap Anda",
              label: "Nama Lengkap",
              icon: "assets/icons/user.svg",
            ),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            EmailFormField(
              hint: "Alamat Email Anda",
              label: "Alamat Email",
              icon: "assets/icons/mail.svg",
            ),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            buildDateFormField(context),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            SizedBox(
              height: getProportionalScreenHeight(20),
            ),
            DefaultButton(
              text: "Lanjut",
              press: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  Navigator.pushNamed(context, RegisterPageJenis.routeName);
                }
              },
            ),
            SizedBox(height: getProportionalScreenHeight(20)),
            Text(
              "Atau Daftar Melalui",
              style: TextStyle(color: kTextGrey),
            ),
            SizedBox(height: getProportionalScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SocialMediaCard(
                  icon: "assets/icons/facebook.svg",
                  press: () {},
                ),
                SocialMediaCard(
                  icon: "assets/icons/google.svg",
                  press: () {},
                ),
              ],
            ),
          ],
        ));
  }

  GestureDetector buildDateFormField(BuildContext context) {
    return GestureDetector(
      child: TextFormField(
        readOnly: true,
        decoration: InputDecoration(
            hintText: "${selectedDate.toLocal()}".split(' ')[0],
            labelText: "Tanggal Lahir",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: CustomSuffixIcon(
              svgIcon: "assets/icons/calendar.svg",
            )),
        onTap: () {
          _selectDate(context);
        },
      ),
    );
  }
}
