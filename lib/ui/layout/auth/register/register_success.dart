part of template;

class RegisterSuccessLayout extends StatelessWidget {
  const RegisterSuccessLayout({Key key}) : super(key: key);
  static String routeName = "/register_succes";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          "Berhasil Daftar",
          style: TextStyle(color: Colors.black54, fontWeight: FontWeight.w600),
        ),
      ),
      body: BodyRegisterSuccess(),
    );
  }
}

class BodyRegisterSuccess extends StatefulWidget {
  const BodyRegisterSuccess({Key key}) : super(key: key);

  @override
  _BodyRegisterSuccess createState() => _BodyRegisterSuccess();
}

class _BodyRegisterSuccess extends State<BodyRegisterSuccess> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 4),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage())));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.screenHeight * 0.08,
          ),
          Image.asset("assets/images/login_success.png",
              height: SizeConfig.screenHeight * 0.4),
          SizedBox(
            height: SizeConfig.screenHeight * 0.08,
          ),
          Text("Berhasil Daftar",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "Balooda",
                  color: kPrimaryColor,
                  fontSize: getProportioanalScreenWidth(36))),
        ],
      ),
    );
  }
}
