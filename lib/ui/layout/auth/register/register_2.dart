part of template;

class RegisterPageJenis extends StatelessWidget {
  const RegisterPageJenis({Key key}) : super(key: key);
  static String routeName = "/register_page2";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: buildAppBarRegister(context),
      body: RegisterBodyHeader(
        imgActive: "assets/images/img_active2.png",
        content: RegisterFormJenis(),
      ),
    );
  }
}
