part of template;

class ForgetBody extends StatelessWidget {
  const ForgetBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportioanalScreenWidth(20)),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.screenHeight * 0.06,
            ),
            Text("Lupa Password",
                style: TextStyle(
                    fontSize: getProportioanalScreenWidth(28),
                    fontFamily: "Balooda",
                    fontWeight: FontWeight.bold)),
            Text(
              "Masukan email anda, kami akan mengirimkan \nlink untuk mengembalikan akun anda.",
              textAlign: TextAlign.center,
              style: TextStyle(color: kTextGrey),
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.1,
            ),
            ForgetPassField()
          ],
        ),
      ),
    );
  }
}

class ForgetPassField extends StatefulWidget {
  const ForgetPassField({Key key}) : super(key: key);

  @override
  _ForgetPassFieldState createState() => _ForgetPassFieldState();
}

class _ForgetPassFieldState extends State<ForgetPassField> {
  final _formKey = GlobalKey<FormState>();
  List<String> errors = [];
  String email;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
              keyboardType: TextInputType.emailAddress,
              onSaved: (newValue) => email = newValue,
              onChanged: (value) {
                if (value.isNotEmpty && errors.contains(kEmailNullError)) {
                  setState(() {
                    errors.remove(kEmailNullError);
                  });
                } else if (emailValidatorRegExp.hasMatch(value) &&
                    errors.contains(kInvalidEmailError)) {
                  setState(() {
                    errors.remove(kInvalidEmailError);
                  });
                }
                return null;
              },
              validator: (value) {
                if (value.isEmpty && !errors.contains(kEmailNullError)) {
                  setState(() {
                    errors.add(kEmailNullError);
                  });
                } else if (!emailValidatorRegExp.hasMatch(value) &&
                    !errors.contains(kInvalidEmailError)) {
                  setState(() {
                    errors.add(kInvalidEmailError);
                  });
                }
                return null;
              },
              decoration: InputDecoration(
                  hintText: "Masukan Email ",
                  labelText: "Email",
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  suffixIcon: CustomSuffixIcon(
                    svgIcon: "assets/icons/mail.svg",
                  ))),
          SizedBox(
            height: getProportionalScreenHeight(30),
          ),
          FormErorr(
            errors: errors,
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.1,
          ),
          DefaultButton(
            text: "Kirim",
            press: () {
              if (_formKey.currentState.validate()) {}
            },
          )
        ],
      ),
    );
  }
}
