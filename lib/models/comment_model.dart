import 'package:meta/meta.dart';

import 'package:quranku/models/models.dart';

class CommentPost {
  final ListFriend user;

  final String timeComment;
  final String commentText;
  final List<String> likedBy;

  CommentPost({
    @required this.user,
    @required this.timeComment,
    @required this.commentText,
    @required this.likedBy,
  });
}
