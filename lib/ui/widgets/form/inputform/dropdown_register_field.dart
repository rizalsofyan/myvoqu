part of template;

DropdownButtonFormField buildDropdownButtonFormField(String pengguna,
    String hint, String label, List<String> _pengguna, Function dropitem) {
  return DropdownButtonFormField(
    items: _pengguna.map((e) {
      return DropdownMenuItem(
        child: Text(e),
        value: e,
      );
    }).toList(),
    value: pengguna,
    onChanged: dropitem,
    decoration: InputDecoration(
        hintText: hint,
        labelText: label,
        floatingLabelBehavior: FloatingLabelBehavior.always),
  );
}
