part of template;

class GroupDetailLayout extends StatefulWidget {
  const GroupDetailLayout({Key key}) : super(key: key);
  static String routeName = "/group_detail";

  @override
  _GroupDetailLayoutState createState() => _GroupDetailLayoutState();
}

class _GroupDetailLayoutState extends State<GroupDetailLayout> {
  int message;
  int messageTugas;
  List<GroupPostModel> post = groupPosts;
  ListFriend user = currentUser1;
  // List<User> liked = orangSuka;
  // int jmlLikedIndex;
  // @override
  // void initState() {
  //   jmlLikedIndex = liked.length;
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final GroupDetailArgumen argument =
        ModalRoute.of(context).settings.arguments;
    return Theme(
      data: ThemeData(
        inputDecorationTheme: null,
        fontFamily: 'Balooda',
        textTheme: const TextTheme(
          bodyText2: TextStyle(fontFamily: 'Balooda'),
        ),
      ),
      child: Scaffold(
        appBar: appBarGroup(context, "Grup Hafalan", true, null,
            //modal more
            () {
          FocusScope.of(context).requestFocus(new FocusNode());
          showModalBottomSheet(
              backgroundColor: Colors.transparent,
              context: context,
              builder: (context) {
                return Stack(
                  children: [
                    Container(
                        height: SizeConfig.screenHeight * 0.5,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20)),
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    top: getProportioanalScreenWidth(50),
                                    left: getProportioanalScreenWidth(10),
                                    right: getProportioanalScreenWidth(10)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                        child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            width: SizeConfig.screenWidth * 0.2,
                                            height:
                                                SizeConfig.screenWidth * 0.2,
                                            decoration: BoxDecoration(
                                              image: argument.groupList.imgGroup
                                                      .isEmpty
                                                  ? DecorationImage(
                                                      fit: BoxFit.contain,
                                                      image: AssetImage(
                                                          "assets/images/background_group_img.png"),
                                                    )
                                                  : DecorationImage(
                                                      fit: BoxFit.cover,
                                                      image: AssetImage(argument
                                                          .groupList.imgGroup),
                                                    ),
                                            )),
                                        SizedBox(
                                            width: getProportioanalScreenWidth(
                                                10)),
                                        Container(
                                            child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                argument.groupList.name
                                                    .toString(),
                                                style: TextStyle(
                                                    fontFamily: "Balooda",
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            Text(
                                                argument.groupList.createdBy
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: kTextGrey)),
                                          ],
                                        )),
                                      ],
                                    )),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right:
                                              getProportioanalScreenWidth(15)),
                                      child: Icon(MdiIcons.information,
                                          color: Colors.blue.withOpacity(0.5),
                                          size: 40),
                                    )
                                  ],
                                )),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: getProportioanalScreenWidth(10),
                                  horizontal: getProportioanalScreenWidth(20)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                // mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                          width: SizeConfig.screenWidth,
                                          height:
                                              getProportioanalScreenWidth(40),
                                          padding: EdgeInsets.symmetric(
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      10)),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Text("Edit Grup"),
                                          ))),
                                  Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                          width: SizeConfig.screenWidth,
                                          height:
                                              getProportioanalScreenWidth(40),
                                          padding: EdgeInsets.symmetric(
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      10)),
                                          child: InkWell(
                                            onTap: () {},
                                            child:
                                                Text("Lihat rekap penugasan"),
                                          ))),
                                  Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                          width: SizeConfig.screenWidth,
                                          height:
                                              getProportioanalScreenWidth(40),
                                          padding: EdgeInsets.symmetric(
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      10)),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Text("Salin tautan grup"),
                                          ))),
                                  Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                          width: SizeConfig.screenWidth,
                                          height:
                                              getProportioanalScreenWidth(40),
                                          padding: EdgeInsets.symmetric(
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      10)),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Text("Bagikan ke..."),
                                          ))),
                                  Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                          width: SizeConfig.screenWidth,
                                          height:
                                              getProportioanalScreenWidth(40),
                                          padding: EdgeInsets.symmetric(
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      10)),
                                          child: InkWell(
                                            onTap: () {},
                                            child: Text(
                                              "Keluar Grup",
                                            ),
                                          ))),

                                  // SizedBox(
                                  //   height: getProportioanalScreenWidth(30),
                                  // ),
                                ],
                              ),
                            )
                          ],
                        )),
                    Positioned(
                        top: 10,
                        left: 50,
                        right: 50,
                        child: Container(
                          width: 5,
                          height: getProportioanalScreenWidth(5),
                          margin: EdgeInsets.symmetric(
                              horizontal: getProportioanalScreenWidth(110)),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.blue.withOpacity(0.7),
                          ),
                        ))
                  ],
                );
              });
        }),
        body: SafeArea(
            child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Container(
                        height: SizeConfig.screenHeight * 0.28,
                        decoration: BoxDecoration(
                          image: argument.groupList.imgGroup.isEmpty
                              ? DecorationImage(
                                  image: AssetImage(
                                      "assets/images/background_group_img.png"),
                                  fit: BoxFit.fill,
                                )
                              : DecorationImage(
                                  image:
                                      AssetImage(argument.groupList.imgGroup),
                                  fit: BoxFit.fill,
                                ),
                        ),
                      ),
                      Container(
                          child: Column(
                        children: [
                          Container(
                            height: SizeConfig.screenHeight * 0.35,
                            child: Column(
                              children: [
                                Flexible(
                                  child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Flexible(
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              top: getProportioanalScreenWidth(
                                                  10),
                                              left: getProportioanalScreenWidth(
                                                  20),
                                              right:
                                                  getProportioanalScreenWidth(
                                                      20)),
                                          width: SizeConfig.screenWidth * 0.8,
                                          height: SizeConfig.screenHeight * 0.1,
                                          child: Column(
                                            children: [
                                              Text(
                                                argument.groupList.name,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: TextStyle(
                                                  fontSize:
                                                      getProportioanalScreenWidth(
                                                          24),
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              // SizedBox(
                                              //   height:
                                              //       getProportioanalScreenWidth(5),
                                              // ),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Dibuat oleh ',
                                                  style: TextStyle(
                                                      fontSize:
                                                          getProportioanalScreenWidth(
                                                              15),
                                                      color: Colors.grey[700],
                                                      fontFamily: "Balooda"),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                            "${argument.groupList.createdBy}",
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: getProportioanalScreenWidth(
                                                23)),
                                        child: RichText(
                                            text: TextSpan(children: [
                                          WidgetSpan(
                                            child: Icon(MdiIcons.account,
                                                size:
                                                    getProportioanalScreenWidth(
                                                        16),
                                                color: kPrimaryColor),
                                          ),
                                          TextSpan(
                                              text: argument
                                                  .groupList.numberMemberCount
                                                  .toString(),
                                              style: TextStyle(
                                                  fontSize:
                                                      getProportioanalScreenWidth(
                                                          14),
                                                  color: Colors.black))
                                        ])),
                                      ),
                                    ],
                                  ),
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      buttonGrupMenu("Informasi", 1,
                                          GroupInfoDetail.routeName, null),
                                      buttonGrupMenuTugas("Tugas", 6,
                                          GroupTaskLayout.routeName),
                                      buttonGrupMenu("Anggota", 0,
                                          GroupMemberLayout.routeName, null),
                                      buttonGrupMenu(
                                          "Obrolan",
                                          0,
                                          GroupChattingLayout.routeName,
                                          GroupChattingArgument(
                                              groupList: argument.groupList)),
                                      buttonGrupMenu("Quiz", 0, null, null),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: getProportioanalScreenWidth(20),
                                    left: getProportioanalScreenWidth(20),
                                    right: getProportioanalScreenWidth(20),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 50,
                                        height: 50,
                                        child: CircleAvatar(
                                            backgroundImage:
                                                CachedNetworkImageProvider(
                                                    user.fotoURl),
                                            radius: 50),
                                      ),
                                      Flexible(
                                          child: Container(
                                        margin: EdgeInsets.only(
                                            left: getProportioanalScreenWidth(
                                                10)),
                                        child: TextField(
                                          decoration: InputDecoration(
                                              hintText: "Unggah Hafalan",
                                              hintStyle: TextStyle(
                                                  fontSize:
                                                      getProportioanalScreenWidth(
                                                          16)),
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 20),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  gapPadding: 5)),
                                        ),
                                      ))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )),
                      Container(
                        // margin: EdgeInsets.only(top: getProportioanalScreenWidth(20)),
                        width: SizeConfig.screenWidth,
                        height: SizeConfig.screenHeight * 0.06,
                        color: Colors.lightBlue[200].withOpacity(0.3),
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: getProportioanalScreenWidth(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Aktivitas grup",
                                  style: TextStyle(
                                      color: kTextGrey,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5))
                            ],
                          ),
                        ),
                      ),
                      //tambagin post layout buat grup
                      ...List.generate(post.length, (index) {
                        return Container(
                            color: Colors.white,
                            child: Column(
                              children: [
                                Container(
                                  child: Column(
                                    children: [
                                      Container(
                                        height: SizeConfig.screenHeight * 0.1,
                                        margin: EdgeInsets.symmetric(
                                            horizontal:
                                                getProportioanalScreenWidth(20),
                                            vertical:
                                                getProportioanalScreenWidth(
                                                    10)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            InkWell(
                                                onTap: () => Navigator.pushNamed(
                                                    context,
                                                    DetailFollower.routeName,
                                                    arguments:
                                                        FollowerDetailArgument(
                                                            listFriend:
                                                                post[index]
                                                                    .user)),
                                                child: Container(
                                                    child: Row(
                                                  children: [
                                                    Container(
                                                        width: 50,
                                                        height: 50,
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(50),
                                                          color: Colors.grey
                                                              .withOpacity(0.8),
                                                        ),
                                                        child: post[index]
                                                                    .user
                                                                    .fotoURl !=
                                                                null
                                                            ? CircleAvatar(
                                                                radius: 50,
                                                                backgroundImage:
                                                                    CachedNetworkImageProvider(
                                                                  post[index]
                                                                      .user
                                                                      .fotoURl,
                                                                ),
                                                              )
                                                            : Icon(
                                                                MdiIcons
                                                                    .account,
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        0.8))),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left:
                                                              getProportioanalScreenWidth(
                                                                  10)),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          SizedBox(
                                                            height: 20,
                                                            child: Text(
                                                                "${post[index].user.name}",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        getProportioanalScreenWidth(
                                                                            18),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600)),
                                                          ),
                                                          SizedBox(
                                                            height: 15,
                                                            child: Text(
                                                                post[index]
                                                                    .lokasi,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        getProportioanalScreenWidth(
                                                                            12),
                                                                    color: Colors
                                                                            .grey[
                                                                        700])),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ))),
                                            IconButton(
                                                icon: Icon(Icons.more_horiz,
                                                    size:
                                                        getProportioanalScreenWidth(
                                                            40)),
                                                onPressed: null)
                                          ],
                                        ),
                                      ),
                                      Container(
                                        width: SizeConfig.screenWidth,
                                        height: SizeConfig.screenHeight * 0.5,
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                fit: BoxFit.fill,
                                                image:
                                                    CachedNetworkImageProvider(
                                                        post[index].fileUrl))),
                                      ),
                                      Container(
                                          height:
                                              SizeConfig.screenHeight * 0.08,
                                          margin: EdgeInsets.symmetric(
                                            horizontal:
                                                getProportioanalScreenWidth(20),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                  child: Row(
                                                children: [
                                                  SvgPicture.asset(
                                                    "assets/icons/love_icon.svg",
                                                    width: 25,
                                                    color: Colors.black,
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        getProportioanalScreenWidth(
                                                            15),
                                                  ),
                                                  SvgPicture.asset(
                                                    "assets/icons/comment_icon.svg",
                                                    color: Colors.black,
                                                    width: 25,
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        getProportioanalScreenWidth(
                                                            15),
                                                  ),
                                                  SvgPicture.asset(
                                                    "assets/icons/message_icon.svg",
                                                    color: Colors.black,
                                                    width: 25,
                                                  ),
                                                ],
                                              )),
                                              Container(
                                                  child: SvgPicture.asset(
                                                "assets/icons/save_icon.svg",
                                                color: Colors.black,
                                                width: 25,
                                              )),
                                            ],
                                          )),
                                      Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal:
                                                  getProportioanalScreenWidth(
                                                      20),
                                              vertical:
                                                  getProportioanalScreenWidth(
                                                      5)),
                                          child: Row(
                                            children: [
                                              Container(
                                                  width: 20,
                                                  height: 20,
                                                  child: CircleAvatar(
                                                    radius: 50,
                                                    backgroundImage:
                                                        CachedNetworkImageProvider(post[
                                                                index]
                                                            .likesBy[Random()
                                                                .nextInt(post[
                                                                        index]
                                                                    .likesBy
                                                                    .length)]
                                                            .fotoURl),
                                                  )),
                                              SizedBox(
                                                width:
                                                    getProportioanalScreenWidth(
                                                        5),
                                              ),
                                              Container(
                                                child: Text.rich(TextSpan(
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        wordSpacing: 0.5),
                                                    children: [
                                                      TextSpan(
                                                        text: "Disukai oleh ",
                                                      ),
                                                      TextSpan(
                                                          text:
                                                              '${post[index].likesBy[Random().nextInt(post[index].likesBy.length)].name} ',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700)),
                                                      TextSpan(
                                                        text: "dan ",
                                                      ),
                                                      TextSpan(
                                                          text:
                                                              "${post[index].likesBy.length} Lainnya",
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w700)),
                                                    ])),
                                              )
                                            ],
                                          )),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                          horizontal:
                                              getProportioanalScreenWidth(20),
                                          vertical:
                                              getProportioanalScreenWidth(10),
                                        ),
                                        child: ExpandableText(
                                          "${post[index].caption}",
                                          style: TextStyle(
                                              letterSpacing: -0.5,
                                              fontSize: 14,
                                              wordSpacing: 0.5),
                                          expandText: "Lainnya",
                                          prefixText:
                                              "${post[index].user.name}",
                                          prefixStyle: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                          collapseText: "Sedikit",
                                          linkColor: kTextGrey,
                                          maxLines: 2,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                          horizontal:
                                              getProportioanalScreenWidth(20),
                                        ),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                "Lihat semua ${post[index].comments} Komentar",
                                                style: TextStyle(
                                                    color: kTextGrey)),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.symmetric(
                                            horizontal:
                                                getProportioanalScreenWidth(20),
                                            vertical:
                                                getProportioanalScreenWidth(10),
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: 30,
                                                height: 30,
                                                child: CircleAvatar(
                                                  radius: 50.0,
                                                  backgroundColor:
                                                      Colors.grey[200],
                                                  backgroundImage:
                                                      CachedNetworkImageProvider(
                                                          post[index]
                                                              .user
                                                              .fotoURl),
                                                ),
                                              ),
                                              SizedBox(
                                                width:
                                                    getProportioanalScreenWidth(
                                                        10),
                                              ),
                                              GestureDetector(
                                                onTap: () {},
                                                child: Text(
                                                  "Tambahkan Komentar...",
                                                  style: TextStyle(
                                                      color: kTextGrey),
                                                ),
                                              ),
                                            ],
                                          )),
                                      Container(
                                          margin: EdgeInsets.symmetric(
                                            horizontal:
                                                getProportioanalScreenWidth(20),
                                          ),
                                          child: Row(
                                            children: [
                                              Text(
                                                '${post[index].timeAgo} yang lalu',
                                                style: TextStyle(
                                                    color: kTextGrey,
                                                    fontSize: 12),
                                              )
                                            ],
                                          ))
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: getProportionalScreenHeight(10),
                                ),
                                Divider(
                                  color: Colors.grey.withOpacity(0.2),
                                  height: 2,
                                )
                              ],
                            ));
                      })
                    ],
                  ),
                )
              ],
            ),
          ),
        )),
      ),
    );
  }

  InkWell buttonGrupMenu(
      String text, int numberMessage, String route, Object argument) {
    return InkWell(
      onTap: () {
        setState(() {
          message = 0;
        });
        Navigator.pushNamed(context, route, arguments: argument);
      },
      child: Stack(
        children: [
          Container(
              margin: EdgeInsets.only(
                  right: getProportioanalScreenWidth(10),
                  top: getProportioanalScreenWidth(20)),
              padding: EdgeInsets.symmetric(
                  vertical: getProportioanalScreenWidth(10),
                  horizontal: getProportioanalScreenWidth(22)),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(20)),
              child: Text(text)),
          numberMessage != 0 && text == "Informasi" && message != 0
              ? Positioned(
                  top: 12,
                  right: 10,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.red,
                    ),
                    child: Center(
                      child: Text(
                        numberMessage.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: getProportioanalScreenWidth(12)),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
    );
  }

  InkWell buttonGrupMenuTugas(String text, int numberMessage, String route) {
    return InkWell(
      onTap: () {
        setState(() {
          messageTugas = 0;
        });
        Navigator.pushNamed(context, route);
      },
      child: Stack(
        children: [
          Container(
              margin: EdgeInsets.only(
                  right: getProportioanalScreenWidth(10),
                  top: getProportioanalScreenWidth(20)),
              padding: EdgeInsets.symmetric(
                  vertical: getProportioanalScreenWidth(10),
                  horizontal: getProportioanalScreenWidth(22)),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(20)),
              child: Text(text)),
          numberMessage != 0 && text == "Tugas" && messageTugas != 0
              ? Positioned(
                  top: 12,
                  right: 10,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.red,
                    ),
                    child: Center(
                      child: Text(
                        numberMessage.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: getProportioanalScreenWidth(12)),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
        ],
      ),
    );
  }
}

class GroupDetailArgumen {
  final GroupListModel groupList;

  GroupDetailArgumen({@required this.groupList});
}
