part of template;

class UploudPhotoForm extends StatefulWidget {
  const UploudPhotoForm({Key key}) : super(key: key);

  @override
  _UploudPhotoFormState createState() => _UploudPhotoFormState();
}

class _UploudPhotoFormState extends State<UploudPhotoForm> {
  File tmpImg;

  Future getImage(ImageSource media) async {
    var img = await ImagePicker.pickImage(source: media);
    setState(() {
      tmpImg = img;
    });
  }

  void alert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text("Silahkan pilih foto anda"),
            content: Container(
              height: SizeConfig.screenHeight * 0.2,
              child: Column(
                children: [
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                        getImage(ImageSource.gallery);
                      },
                      child: Row(
                        children: [
                          Icon(Icons.image),
                          Text("Ambil dari Galeri"),
                        ],
                      )),
                  FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                        getImage(ImageSource.camera);
                      },
                      child: Row(
                        children: [
                          Icon(Icons.camera),
                          Text("Ambil dari Kamera"),
                        ],
                      ))
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(children: [
        SizedBox(height: getProportionalScreenHeight(20)),
        Padding(
            padding: EdgeInsets.only(left: 16, top: 16, right: 16),
            child: tmpImg == null
                ? GestureDetector(
                    onTap: () {
                      alert();
                    },
                    child: Image(
                      image: AssetImage("assets/images/profile.png"),
                      width: SizeConfig.screenWidth * 0.4,
                    ),
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.file(
                      tmpImg,
                      fit: BoxFit.cover,
                      width: SizeConfig.screenHeight,
                      height: SizeConfig.screenHeight / 5,
                    ),
                  )),
        SizedBox(
          height: SizeConfig.screenHeight * 0.08,
        ),
        DefaultButton(
          press: () {
            Navigator.pushNamed(context, TermConditionPage.routeName);
          },
          text: "Lanjutkan",
        )
      ]),
    );
  }
}
