part of template;

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);
  static String routeName = "/homepage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectIndex = 0;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      drawer: Container(
          width: SizeConfig.screenWidth,
          child: SideBarMenu(
            currentUser: currentUser,
          )),
      appBar: buildHomepageAppbar(context, 7),
      body: SafeArea(
          child: IndexedStack(
        index: selectIndex,
        children: [
          CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                sliver: SliverToBoxAdapter(
                  child: ListOnline(onlineUser: onlineUser),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  final Post post = posts[index];
                  return PostLayout(post: post);
                }, childCount: posts.length),
              )
            ],
          ),
          Center(
            child: Text("2"),
          ),
          Center(
            child: Text("3"),
          ),
          Center(
            child: Text("4"),
          ),
          Center(
            child: Text("5"),
          ),
        ],
      )),
      bottomNavigationBar: bottomNavTab(),
    );
  }

  Widget bottomNavTab() {
    User user = currentUser;
    List iconBottom = [
      Icon(
        Icons.home,
        color: kPrimaryColor,
        // size: 24,
      ),
      SvgPicture.asset(
        "assets/icons/feed-icon.svg",
        color: kPrimaryColor,
        // width: 20,
      ),
      SvgPicture.asset(
        "assets/icons/add.svg",
        width: 27,
        color: kPrimaryColor,
      ),
      Icon(
        Icons.notifications,
        color: kPrimaryColor,
        // size: 24,
      ),
      CircleAvatar(
        // radius: 25,
        backgroundColor: Colors.grey[200],
        backgroundImage: CachedNetworkImageProvider(user.imageUrl),
      ),
    ];
    return Container(
        width: double.infinity,
        height: 60,
        decoration: BoxDecoration(color: Colors.white),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                  5,
                  (index) => IconButton(
                        icon: iconBottom[index],
                        onPressed: () {
                          setState(() {
                            selectIndex = index;
                          });
                        },
                      ))),
        ));
  }
}

class ListOnline extends StatelessWidget {
  const ListOnline({Key key, this.onlineUser}) : super(key: key);
  final List<User> onlineUser;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      // color: Colors.orange,
      child: ListView.builder(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 4.0),
          scrollDirection: Axis.horizontal,
          itemCount: 1 + onlineUser.length,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    children: [
                      _CreateListDetail(),
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        height: 40.0,
                        width: 1.5,
                        color: Colors.grey[300],
                      )
                    ],
                  ));
            }
            final User user = onlineUser[index - 1];
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: ProfileAvatar(
                imgUrl: user.imageUrl,
                isActive: true,
                name: user.name,
              ),
            );
          }),
    );
  }
}

class _CreateListDetail extends StatelessWidget {
  const _CreateListDetail({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      // color: Colors.black,
      onPressed: () => print("open list"),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      // borderSide: BorderSide(width: 2.0, color: kPrimaryColor),
      textColor: Colors.white,
      child: Row(
        children: [
          Text("Tampilkan Semua"),
        ],
      ),
      color: Colors.blueAccent,
    );
  }
}

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar(
      {Key key,
      this.imgUrl,
      this.isActive = false,
      this.name,
      this.like = false})
      : super(key: key);
  final String imgUrl;
  final bool isActive;
  final String name;
  final bool like;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 15.0,
          backgroundColor: Colors.grey[200],
          backgroundImage: CachedNetworkImageProvider(imgUrl),
        ),
        isActive
            ? Positioned(
                bottom: 10.0,
                right: 0.0,
                child: Container(
                  width: 10.0,
                  height: 10.0,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      shape: BoxShape.circle,
                      border: Border.all(width: 1.0, color: Colors.white)),
                ),
              )
            : const SizedBox.shrink(),
        name != null
            ? Positioned(
                bottom: -2,
                child: Container(
                  // margin: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    name,
                    style: TextStyle(fontSize: 10),
                  ),
                ))
            : const SizedBox.shrink(),
      ],
    );
  }
}

class PostLayout extends StatelessWidget {
  const PostLayout({
    Key key,
    @required this.post,
  }) : super(key: key);
  final Post post;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5.0),
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      // height: 100.0,
      color: Colors.grey[50],
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              HeaderPost(post: post),
              const SizedBox(height: 4.0),
              post.imgUrl != null
                  ? const SizedBox.shrink()
                  : SizedBox(
                      height: 6.0,
                    )
              // BodyPost(),
            ],
          ),
          post.imgUrl != null
              ? CachedNetworkImage(
                  imageUrl: post.imgUrl,
                  fit: BoxFit.cover,
                )
              : SizedBox.shrink(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: Column(
              children: [
                SizedBox(
                  height: getProportionalScreenHeight(10),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/love_icon.svg",
                          width: 27,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: getProportioanalScreenWidth(10),
                        ),
                        SvgPicture.asset(
                          "assets/icons/comment_icon.svg",
                          color: Colors.black,
                          width: 27,
                        ),
                        SizedBox(
                          width: getProportioanalScreenWidth(10),
                        ),
                        SvgPicture.asset(
                          "assets/icons/message_icon.svg",
                          color: Colors.black,
                          width: 27,
                        )
                      ],
                    ),
                    SvgPicture.asset(
                      "assets/icons/save_icon.svg",
                      color: Colors.black,
                      width: 27,
                    )
                  ],
                ),
                SizedBox(
                  height: getProportionalScreenHeight(10),
                ),
                PeopleLike(
                  orangSuka: orangSuka[1],
                ),
                SizedBox(
                  height: getProportionalScreenHeight(10),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ExpandableText(
                      '${post.caption}',
                      style: TextStyle(fontSize: 14),
                      expandText: "Lainnya",
                      prefixText: '${post.user.name}',
                      prefixStyle:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      maxLines: 2,
                      collapseText: "Sedikit",
                      linkColor: kTextGrey,
                    ),
                  ],
                ),
                SizedBox(
                  height: getProportionalScreenHeight(20),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Text("Lihat semua ${post.comments} komentar",
                            style: TextStyle(color: kTextGrey)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: getProportionalScreenHeight(20),
                ),
                Row(
                  children: [
                    CircleAvatar(
                      radius: 15.0,
                      backgroundColor: Colors.grey[200],
                      backgroundImage:
                          CachedNetworkImageProvider(post.user.imageUrl),
                    ),
                    SizedBox(
                      width: getProportioanalScreenWidth(10),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Text(
                        "Tambahkan Komentar",
                        style: TextStyle(color: kTextGrey),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: getProportionalScreenHeight(10),
                ),
                Row(
                  children: [
                    Text(
                      '${post.timeAgo} yang lalu',
                      style: TextStyle(color: kTextGrey, fontSize: 12),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class HeaderPost extends StatelessWidget {
  const HeaderPost({
    Key key,
    @required this.post,
  }) : super(key: key);
  final Post post;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ProfileAvatar(imgUrl: post.user.imageUrl),
        const SizedBox(
          width: 8.0,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${post.user.name}",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              Row(
                children: [
                  Text('${post.lokasi}',
                      style: TextStyle(fontSize: 11, color: Colors.grey[700])),
                ],
              )
            ],
          ),
        ),
        IconButton(
            icon: const Icon(Icons.more_horiz),
            onPressed: () {
              print('more');
            })
      ],
    );
  }
}

class PeopleLike extends StatelessWidget {
  const PeopleLike({
    Key key,
    this.orangSuka,
  }) : super(key: key);
  final User orangSuka;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          radius: 15.0,
          backgroundColor: Colors.grey[200],
          backgroundImage: CachedNetworkImageProvider(orangSuka.imageUrl),
        ),
        SizedBox(
          width: 8.0,
        ),
        Text.rich(TextSpan(children: [
          TextSpan(
              text: "Disukai oleh ",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
          TextSpan(
              text: '${orangSuka.name} ',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700)),
          TextSpan(
              text: "dan ",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
          TextSpan(
              text: "Lainnya",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700)),
        ])),
      ],
    );
  }
}
