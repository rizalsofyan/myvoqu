part of template;

AppBar buildAppBarLogin(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.white,
    leading: IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pushNamed(context, LandingPage.routeName);
      },
      color: kPrimaryColor,
    ),
    centerTitle: true,
    elevation: 0,
    title: Text(
      "MyVoQu",
      style: TextStyle(
          fontFamily: 'Balooda',
          fontWeight: FontWeight.bold,
          fontSize: getProportioanalScreenWidth(34),
          color: kPrimaryColor),
    ),
    actions: [
      IconButton(
          constraints: BoxConstraints.expand(width: 80),
          icon: Text(
            'Daftar',
            textAlign: TextAlign.center,
            style: TextStyle(color: kPrimaryColor),
          ),
          onPressed: () {
            Navigator.pushNamed(context, RegisterPage.routeName);
          })
    ],
  );
}
