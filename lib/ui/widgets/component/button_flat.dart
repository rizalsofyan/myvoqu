part of template;

class FlatButtonCustom extends StatelessWidget {
  const FlatButtonCustom({
    Key key,
    this.press,
    this.color,
    this.shape,
    this.text,
    this.colorText,
  }) : super(key: key);
  final Function press;
  final Color color;
  final ShapeBorder shape;
  final String text;
  final Color colorText;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
        onPressed: press,
        color: color,
        shape: shape,
        child: Text(
          text,
          style: TextStyle(color: colorText),
        ));
  }
}
