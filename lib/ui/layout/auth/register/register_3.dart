part of template;

class RegisterPagePassword extends StatelessWidget {
  const RegisterPagePassword({Key key}) : super(key: key);
  static String routeName = '/register_password';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: buildAppBarRegister(context),
      body: RegisterBodyHeader(
        imgActive: "assets/images/img_active3.png",
        content: RegisterFormPassword(),
      ),
    );
  }
}
