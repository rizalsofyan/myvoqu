part of 'shared.dart';

//Color
Color kPrimaryColor = "1DA1F2".toColor();
Color kPrimaryLightColor = "BBE5FF".toColor();
Color kTextGrey = "8E8E8E".toColor();
Color kGroupColorBox = "3F82CF".toColor();
Color kNumberTextGroup = "001B7A".toColor();
Color kTextNavColor = "DDDDDD".toColor();
Color kIconSignColor = "65C5FF".toColor();
Color kSearchBoxColor = "F3F3F3".toColor();
Color kSettingColor = "AEE0FF".toColor();

// animation
const kAnimationDuration = Duration(milliseconds: 200);

ThemeData theme() {
  return ThemeData(
      scaffoldBackgroundColor: Colors.white,
      primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      inputDecorationTheme: inputDecorationTheme());
}

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(28),
    borderSide: BorderSide(color: kTextGrey),
    gapPadding: 10,
  );
  return InputDecorationTheme(
      // hintText: "Telepon atau Alamat Email",
      // labelText: "telepon/email",
      enabledBorder: outlineInputBorder,
      focusedBorder: outlineInputBorder,
      border: outlineInputBorder,
      contentPadding: EdgeInsets.symmetric(horizontal: 40, vertical: 20));
}

//Form error

final RegExp emailValidatorRegExp =
    RegExp(r"[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final String kEmailNullError = "Masukan Email";
final String kInvalidEmailError = "Email Tidak Valid";
final String kPassNullError = "Masukan Password";
final String kInvalidPassError = "Password Salah";
final String kInvalidName = "Masukan Nama";

final headingRegister = TextStyle(
  fontSize: getProportioanalScreenWidth(28),
  fontFamily: "Balooda",
  fontWeight: FontWeight.bold,
  height: 1.5,
);
