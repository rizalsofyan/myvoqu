part of template;

class DetailFollower extends StatefulWidget {
  const DetailFollower({Key key}) : super(key: key);
  static String routeName = "/detail_follower";

  @override
  _DetailFollowerState createState() => _DetailFollowerState();
}

class _DetailFollowerState extends State<DetailFollower> {
  int selectedIndex = 0;
  bool isFollow = false;
  int selectFollowIndex = 0;
  @override
  Widget build(BuildContext context) {
    final FollowerDetailArgument argument =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: buildAppBarUser(context, argument, argument.listFriend.nickname,
            true, false, () {}),
        body: WillPopScope(
          onWillPop: () {
            setState(() {
              selectFollowIndex == 0
                  ? Navigator.pop(context)
                  : selectFollowIndex = 0;
            });
          },
          child: IndexedStack(
            index: selectFollowIndex,
            children: [
              Container(
                margin: EdgeInsets.only(top: getProportioanalScreenWidth(15)),
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: [
                        buildRowProfileHeader(argument.listFriend.fotoURl,
                            argument.listFriend.name),
                        SizedBox(
                          height: getProportionalScreenHeight(40),
                        ),
                        buildRowFollowCount(
                            "${argument.listFriend.foto.length + argument.listFriend.vidio.length + argument.listFriend.ditandai.length}",
                            "${argument.listFriend.follower}",
                            "${argument.listFriend.following}"),
                        SizedBox(
                          height: getProportionalScreenHeight(30),
                        ),
                        buildRowButton(),
                        SizedBox(
                          height: getProportionalScreenHeight(20),
                        ),
                        buildPaddingTabbarMenu(),
                        buildColumnActiveBar(),
                        SingleChildScrollView(
                          child: IndexedStack(
                            index: selectedIndex,
                            children: [
                              getPost(listFriends.length),
                              getPost(listFriends.length),
                              getPost(listFriends.length)
                            ],
                          ),
                        )
                      ],
                    )),
              ),
              FindFollower(),
              FindFollowing(),
            ],
          ),
        ));
  }

  Row buildRowProfileHeader(String img, String textName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            CircleImageRound(
              imgUrl: img,
              radius: 50,
            ),
            SizedBox(
              height: getProportionalScreenHeight(10),
            ),
            Text(
              textName,
              style: TextStyle(
                  fontSize: getProportioanalScreenWidth(23),
                  fontWeight: FontWeight.w600),
            ),
          ],
        )
      ],
    );
  }

  Row buildRowFollowCount(
      String valuePost, String valueFollower, String valueFollowing) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  selectFollowIndex = 0;
                });
              },
              child: FollowerWidget(
                textValue: valuePost,
                title: "Postingan",
              ),
            ),
          ],
        ),
        SizedBox(
          width: getProportioanalScreenWidth(30),
        ),
        Column(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  selectFollowIndex = 1;
                });
              },
              child: FollowerWidget(
                // textValue: "${widget.userBaru.follower}",
                textValue: valueFollower,
                title: "Pengikut",
              ),
            ),
          ],
        ),
        SizedBox(
          width: getProportioanalScreenWidth(30),
        ),
        Column(
          children: [
            GestureDetector(
                onTap: () {
                  setState(() {
                    selectFollowIndex = 2;
                  });
                },
                child: FollowerWidget(
                  // textValue: "${widget.userBaru.following}",
                  textValue: valueFollowing,
                  title: "Mengikuti",
                )),
          ],
        ),
      ],
    );
  }

  Row buildRowButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: SizeConfig.screenWidth * 0.4,
          child: FlatButtonCustom(
            press: () {
              setState(() {
                isFollow = !isFollow;
              });
            },
            color: isFollow ? Colors.white : kPrimaryColor,
            shape: isFollow
                ? RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                    side: BorderSide(width: 0.3))
                : RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
            text: isFollow ? "Mengikuti" : "Ikuti",
            colorText: isFollow ? Colors.black : Colors.white,
          ),
        ),
        SizedBox(
          width: getProportioanalScreenWidth(25),
        ),
        Container(
          width: SizeConfig.screenWidth * 0.4,
          child: OutlineButtonCustom(),
        )
      ],
    );
  }

  Padding buildPaddingTabbarMenu() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 0;
              });
            },
            color: selectedIndex == 0 ? kPrimaryColor : Colors.black,
            widthSize: 0.2,
            text: "Hafalan",
          ),
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 1;
              });
            },
            color: selectedIndex == 1 ? kPrimaryColor : Colors.black,
            widthSize: 0.4,
            text: "Foto/Vidio",
          ),
          TabbarDetailCustom(
            press: () {
              setState(() {
                selectedIndex = 2;
              });
            },
            color: selectedIndex == 2 ? kPrimaryColor : Colors.black,
            widthSize: 0.2,
            text: "Ditandai",
          ),
        ],
      ),
    );
  }

  Column buildColumnActiveBar() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.3,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 0 ? kPrimaryColor : Colors.transparent),
            ),
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.4,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 1 ? kPrimaryColor : Colors.transparent),
            ),
            Container(
              height: 3,
              width: SizeConfig.screenWidth * 0.3,
              decoration: BoxDecoration(
                  color:
                      selectedIndex == 2 ? kPrimaryColor : Colors.transparent),
            )
          ],
        ),
        Container(
          height: 0.5,
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(color: Colors.black.withOpacity(0.8)),
        )
      ],
    );
  }

  Widget getPost(int listFriend) {
    return Container(
      margin: EdgeInsets.only(top: getProportioanalScreenWidth(3)),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: 3,
        runSpacing: 3,
        children: [
          ...List.generate(listFriend, (index) {
            return Container(
              height: 150,
              width: (SizeConfig.screenWidth - 6) / 3,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(listFriends[index].fotoURl),
                      fit: BoxFit.cover)),
            );
          })
        ],
      ),
    );
  }
}

class FollowerDetailArgument {
  final ListFriend listFriend;

  FollowerDetailArgument({@required this.listFriend});
}
