part of template;

class RegisterPagePhoto extends StatelessWidget {
  const RegisterPagePhoto({Key key}) : super(key: key);
  static String routeName = "/register_photo";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: buildAppBarRegister(context),
      body: RegisterBodyHeader(
        imgActive: "assets/images/img_active4.png",
        content: UploudPhotoForm(),
      ),
    );
  }
}
