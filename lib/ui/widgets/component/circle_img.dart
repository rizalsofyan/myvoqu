part of template;

class CircleImageRound extends StatelessWidget {
  const CircleImageRound({
    Key key,
    @required this.imgUrl,
    @required this.radius,
  }) : super(key: key);

  final String imgUrl;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundColor: Colors.grey[200],
      backgroundImage: CachedNetworkImageProvider(imgUrl),
    );
  }
}
