import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quranku/ui/template.dart';

import 'shared/shared.dart';
import 'services/routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MyVoQu',
      theme: theme(),
      // initialRoute: SplashScreen.routeName,
      initialRoute: HomePage.routeName,
      routes: routes,
    );
  }
}
