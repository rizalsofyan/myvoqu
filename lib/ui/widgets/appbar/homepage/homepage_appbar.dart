part of template;

AppBar buildHomepageAppbar(BuildContext context, int jmlMsg) {
  return AppBar(
    // iconTheme: IconThemeData(color: Colors.black),
    backgroundColor: Colors.white,
    centerTitle: true,
    toolbarHeight: getProportionalScreenHeight(100),
    iconTheme: IconThemeData(color: Colors.black),
    title: Text(
      "MyVoQu",
      style: TextStyle(
          fontFamily: 'Balooda',
          fontWeight: FontWeight.w700,
          fontSize: getProportioanalScreenWidth(38),
          color: kPrimaryColor,
          letterSpacing: -1.5),
    ),
    elevation: 0,
    actions: [
      Stack(
        alignment: Alignment.center,
        children: [
          IconButton(
              icon: SvgPicture.asset(
                "assets/icons/send.svg",
                color: Colors.black,
                // size: getProportioanalScreenWidth(28),
              ),
              onPressed: () {}),
          jmlMsg != 0
              ? Positioned(
                  top: 20,
                  right: 0,
                  child: Container(
                    width: 15.0,
                    height: 15.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      '${jmlMsg.toString()}',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12),
                    ),
                  ))
              : SizedBox.shrink(),
        ],
      ),
      SizedBox(
        width: getProportioanalScreenWidth(10),
      )
    ],
  );
}

class MenuHomeSidebar extends StatelessWidget {
  const MenuHomeSidebar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
