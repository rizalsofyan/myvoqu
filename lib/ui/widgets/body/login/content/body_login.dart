part of template;

class LoginFormContent extends StatefulWidget {
  const LoginFormContent({Key key}) : super(key: key);

  @override
  _LoginFormContentState createState() => _LoginFormContentState();
}

class _LoginFormContentState extends State<LoginFormContent> {
  final _formKey = GlobalKey<FormState>();
  String email;
  String password;
  bool remmemberMe = false;
  final List<String> errors = [];
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(
            height: SizeConfig.screenHeight * 0.04,
          ),
          buildPasswordFormField(),
          SizedBox(
            height: getProportionalScreenHeight(20),
          ),
          Row(
            children: [
              Checkbox(
                  value: remmemberMe,
                  activeColor: kPrimaryColor,
                  onChanged: (value) {
                    setState(() {
                      remmemberMe = value;
                    });
                  }),
              Text(
                "Ingat Saya",
                style: TextStyle(color: kTextGrey),
              ),
              Spacer(),
              GestureDetector(
                onTap: () => Navigator.pushNamed(
                    context, ForgetPasswordLayout.routeName),
                child: Text(
                  "Lupa Password ?",
                  style: TextStyle(
                      decoration: TextDecoration.underline, color: kTextGrey),
                ),
              )
            ],
          ),
          FormErorr(errors: errors),
          SizedBox(
            height: getProportionalScreenHeight(10),
          ),
          DefaultButton(
            text: "Masuk",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();

                //succes login
                Navigator.pushNamed(context, LoginSuccessLayout.routeName);
              }
            },
          )
        ],
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
        keyboardType: TextInputType.emailAddress,
        onSaved: (newValue) => email = newValue,
        onChanged: (value) {
          if (value.isNotEmpty && errors.contains(kEmailNullError)) {
            setState(() {
              errors.remove(kEmailNullError);
            });
          } else if (emailValidatorRegExp.hasMatch(value) &&
              errors.contains(kInvalidEmailError)) {
            setState(() {
              errors.remove(kInvalidEmailError);
            });
          }
          return null;
        },
        validator: (value) {
          if (value.isEmpty && !errors.contains(kEmailNullError)) {
            setState(() {
              errors.add(kEmailNullError);
            });
          } else if (!emailValidatorRegExp.hasMatch(value) &&
              !errors.contains(kInvalidEmailError)) {
            setState(() {
              errors.add(kInvalidEmailError);
            });
          }
          return null;
        },
        decoration: InputDecoration(
            hintText: "Masukan Email ",
            labelText: "Email",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: CustomSuffixIcon(
              svgIcon: "assets/icons/mail.svg",
            )));
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
        obscureText: true,
        onSaved: (newValue) => password = newValue,
        onChanged: (value) {
          if (value.isNotEmpty && errors.contains(kPassNullError)) {
            setState(() {
              errors.remove(kPassNullError);
            });
          }
        },
        validator: (value) {
          if (value.isEmpty && !errors.contains(kPassNullError)) {
            setState(() {
              errors.add(kPassNullError);
            });
            return "";
          }

          return null;
        },
        decoration: InputDecoration(
            hintText: "Masukan Password ",
            labelText: "Password",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: CustomSuffixIcon(
              svgIcon: "assets/icons/lock-on.svg",
            )));
  }
}
